HƯỚNG DẪN CÀI ĐẶT VÀ CẤU HÌNH

+ Cài đặt XAMPP:
- Download XAMPP theo đường dẫn: https://downloadsapachefriends.global.ssl.fastly.net/xampp-files/5.6.28/xampp-win32-5.6.28-1-VC11-installer.exe
- Cài đặt XAMPP trên máy chủ. Đến bước chọn nơi lưu XAPP a nên để ổ khác ổ C nhé (thay chữ C thành chữ D hoặc ổ khác nếu muốn, ví dụ ổ G), vì ổ C là ổ chứa hệ điều hành, khi anh cài lại window sẽ mất hết dữ liệu. Các cái khác a để mặc định, cứ bấm next next...finish là xong.
- Nếu a muốn đổi cổng 80 sang cổng khác thì mở file D:/xampp/xampp-control.exe. Trên dòng có ghi chữ Apache, a bấm vào nút config, chọn Apache (httpd-conf), tìm và thay thế (bấm Ctrl + H) số 80 thành 81 (hoặc cổng anh thích). Chi tiết hướng dẫn đổi cổng a có thể xem tại đây (có hình ảnh minh họa): http://www.shopitc-vietnam.com/how-to/thu-thuat-joomla/331-cach-thay-doi-cong-443-va-80-localhost-tren-xampp.html
- Mở Xampp control (theo đường dẫn D:/xampp/xampp-control.exe) , bấm nút Start ở dòng Apache và dòng MySql

+ Cài đặt Code:
- A tải file đính kèm tracnghiem.zip về máy
​
 tracnghiem.zip
​
- Mở thư mục D:/xampp/htdocs (giả sử a lưu XAMPP vào ổ D) trên máy chủ, xóa toàn bộ file trong thư mục htdocs. Sau đó copy file tracnghiem.zip vào thư mục htdocs, giải nén ra (bấm chuột phải, chọn Extract Here).

+ Tạo CSDL:
- Truy cập vào đường dẫn http://localhost/phpmyadmin trên máy chủ
- Trên thanh Sidebar bên trái, a bấm vào New, nhập database name là tracnghiem, collection a chọn utf8_unicode_ci, sau đó bấm nút Create.

- A thanh menu, a vào Import. Bấm vào nút Choose file, chọn file tracnghiem.sql theo đường dẫn D:/xampp/htdocs/tracnghiem.sql. Sau đó, a kéo con lăn xuống cuối trang bấm nút Go.

+ Kiểm tra địa chỉ IP của máy chủ và chạy thử website
- A vào Start => Run (hoặc bấm phím tắt Window + R), gõ CMD rồi bấm Enter. A gõ lệnh ipconfig rồi bấm Enter. Dòng IPv4 Address chính là địa chỉ IP của máy chủ, ví dụ 192.168.0.5.
- A nhập địa chỉ ip vửa kiểm tra vào trình duyệt của máy chủ hoặc máy con để chạy website, ví dụ http://192.16.0.5, nếu a đổi công 80 sang cổng khác thì gõ thêm :cổng, ví dụ http://192.16.0.5:81. Chi tiết về lệnh ipconfig a có thể xem tại đây: http://thuviencntt.com/bai-viet-huong-dan/window/cach-dung-lenh-ipconfig
