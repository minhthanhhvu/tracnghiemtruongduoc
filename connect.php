<?php
ob_start();
session_start();//Khoi động session
date_default_timezone_set('Asia/Ho_Chi_Minh');

//Nhúng thư viện smarty vào website
require('libs/Smarty.class.php');
require('model/thuvien.php');
$smarty=new Smarty;//Khởi tạo đối tượng smarty
$smarty->template_dir='templates/user/';//Trỏ lại đường dẫn giao diện trang người dùng
$smarty->configLoad('cauhinh.dat');//Đọc file cấu hình
$cf=$smarty->getConfigVars();//Lấy tất cả các biến cấu hình
$lib=new LIB($cf);//khởi tạo thư viện LIB
$smarty->assign('lib',$lib);//Khai báo biến lib tầng View

//Lấy ra danh sách cài đặt
$caidat=array();
$danhsach=$lib->selectall("SELECT * FROM tbcaidat WHERE caidat_trangthai=1 AND caidat_parent<>0",false);
foreach ($danhsach as $item){
	$caidat[$item['caidat_ma']]=$item['caidat_noidung'];
}
$_SESSION['caidat']=$caidat;
$smarty->assign('caidat', $caidat);
?>