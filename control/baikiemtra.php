<?php
	//Lấy ra câu hỏi kiểm tra
	if(isset($_SESSION['login'])){
		$taikhoan_id=$_SESSION['login']['taikhoan_id'];
		$nhomtaikhoan_id=$_SESSION['login']['nhomtaikhoan_id'];
		$truong_id=$_SESSION['login']['truong_id'];
		$khoi_id=$_SESSION['login']['khoi_id'];
		$lop_id=$_SESSION['login']['lop_id'];
		
		//Lấy ra danh sách bài kiểm tra định kỳ (có phân trang)
		$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
		//$strdinhky="SELECT * FROM tbkiemtra WHERE (kiemtra_id IN (SELECT kiemtra_id FROM tbnhomtaikhoankiemtra WHERE nhomtaikhoan_id=$nhomtaikhoan_id) OR kiemtra_id IN (SELECT kiemtra_id FROM tbtaikhoankiemtra WHERE taikhoan_id=$taikhoan_id)) AND kiemtra_yeucau=0 ";
		$strdinhky="SELECT * FROM tbkiemtra WHERE kiemtra_trangthai=1 AND kiemtra_id IN (SELECT DISTINCT kiemtra_id FROM tbkiemtra_phongthi WHERE phongthi_id = ".$_SESSION['login']['phongthi_id'].") ";
		$link='index.php?pg=baikiemtra';//Đường link phân trang
		if(isset($_GET['show']) && !empty($_GET['show'])){
			$show=$_GET['show'];
			$link.='&show='.$show;
			$smarty->assign('show',$show);
		}
		
		if(isset($_GET['txtkey'])){
			$txtkey=$_GET['txtkey'];
			$strdinhky.=" AND kiemtra_tieude like '%$txtkey%'";
			$link.='&txtkey='.$txtkey;
			$smarty->assign('txtkey',$txtkey);
		}
		
		if(isset($_GET['txtbatdau']) && !empty($_GET['txtbatdau'])){
			$txtbatdau=$lib->date_convert($_GET['txtbatdau'],'yyyy-mm-dd hh:ii:ss');
			$strdinhky.=" AND kiemtra_ngaytao >= '$txtbatdau' ";
			$link.='&txtbatdau='.$txtbatdau;
			$smarty->assign('txtbatdau',$txtbatdau);
		}
		
		if(isset($_GET['txtketthuc']) && !empty($_GET['txtketthuc'])){
			$txtketthuc=$lib->date_convert($_GET['txtketthuc'],'yyyy-mm-dd hh:ii:ss');
			$strdinhky.=" AND kiemtra_ngaytao <= '$txtketthuc' ";
			$link.='&txtketthuc='.$txtketthuc;
			$smarty->assign('txtketthuc',$txtketthuc);
		}
		
		if(isset($_GET['lsttohop']) && !empty($_GET['lsttohop'])){
			$lsttohop=$_GET['lsttohop'];
			$strdinhky.=" AND tohop_id=$lsttohop ";
			$link.='&lsttohop='.$lsttohop;
			$smarty->assign('lsttohop',$lsttohop);
		}
		
		if(isset($_GET['lsttruong']) && !empty($_GET['lsttruong'])){
			$lsttruong=$_GET['lsttruong'];
			$strdinhky.=" AND taikhoan_id IN (SELECT taikhoan_id FROM tbtaikhoan WHERE truong_id=$lsttruong) ";
			$link.='&lstmucdo='.$lsttruong;
			$smarty->assign('lsttruong',$lsttruong);
		}
		
		if(isset($_GET['lstkhoi']) && !empty($_GET['lstkhoi'])){
			$lstkhoi=$_GET['lstkhoi'];
			$strdinhky.=" AND taikhoan_id IN (SELECT taikhoan_id FROM tbtaikhoan WHERE khoi_id=$lstkhoi) ";
			$link.='&lstkhoi='.$lstkhoi;
			$smarty->assign('lstkhoi',$lstkhoi);
		}
		
		if(isset($_GET['lstlop']) && !empty($_GET['lstlop'])){
			$lstlop=$_GET['lstlop'];
			$strdinhky.=" AND taikhoan_id IN (SELECT taikhoan_id FROM tbtaikhoan WHERE lop_id=$lstlop) ";
			$link.='&lstlop='.$lstlop;
			$smarty->assign('lstlop',$lstlop);
		}
		
		$strdinhky.=" ORDER BY kiemtra_id DESC";
		$dinhky=$lib->selectall($strdinhky);
		$smarty->assign('dinhky',$dinhky);//Khai báo biến tầng view
		$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
		
		//Lấy ra danh sách bài kiểm tra theo yêu cầu (không phân trang)
		$stryeucau="SELECT * FROM tbkiemtra WHERE (kiemtra_id IN (SELECT kiemtra_id FROM tbnhomtaikhoankiemtra WHERE nhomtaikhoan_id=$nhomtaikhoan_id) OR kiemtra_id IN (SELECT kiemtra_id FROM tbtaikhoankiemtra WHERE taikhoan_id=$taikhoan_id)) AND kiemtra_yeucau=1 ORDER BY kiemtra_id DESC";
		$yeucau=$lib->selectall($stryeucau,false);
		$smarty->assign('yeucau',$yeucau);
		$smarty->display('baikiemtra.html');
	}else{
		$lib->redirect('dang-nhap.html');
	}?>