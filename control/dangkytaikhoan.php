<?php
if(isset($_POST['btnreg'])){//Nếu có bấm nút
	$tendangnhap=trim(strtolower($_POST['txtuser']));
	$matkhau1=trim($_POST['txtpass']);
	$matkhau=md5($matkhau1);//Đưa mật khẩu vào mã hóa
	$nhaplaimk=$_POST['txtrepass'];
	$dienthoai=trim($_POST['txtphone']);
	$email=trim($_POST['txtemail']);
	$hoten=trim($_POST['txthoten']);
	
	$diachi=$_POST['txtdiachi'];
	$gioitinh=$_POST['lstgioitinh'];
	$ngaysinh=$lib->date_convert($_POST['txtngaysinh'],'yyyy-mm-dd');
	if(!empty($_POST['lsttruong']))
		$truong=$_POST['lsttruong'];
	else
		$truong=NULL;
	if(!empty($_POST['lstkhoi']))
		$khoi=$_POST['lstkhoi'];
	else
		$khoi=NULL;
	if(!empty($_POST['lstlop']))
		$lop=$_POST['lstlop'];
	else
		$lop=NULL;
	
	$smarty->assign('post',$_POST);
	$errors = array();
	
	$strtaikhoan="SELECT taikhoan_user FROM tbtaikhoan WHERE taikhoan_user='$tendangnhap' AND taikhoan_trangthai=1 ";
	$taikhoan=$lib->selectone($strtaikhoan);
	if(empty($tendangnhap)) {
		$errors[] = 'tendangnhap_rong';
	}elseif(!(preg_match('/^[a-z0-9_]{4,30}$/', $tendangnhap) && preg_match('/\w/', $tendangnhap))){
		$errors[] = 'tendangnhap_sai';
	}elseif(!empty($taikhoan)){
		$errors[] = 'tendangnhap_trung';
	}
	
	$strtaikhoan="SELECT taikhoan_phone FROM tbtaikhoan WHERE taikhoan_phone='$dienthoai' AND taikhoan_trangthai=1 ";
	$taikhoan=$lib->selectone($strtaikhoan);
	if(empty($dienthoai)) {
		$errors[] = 'dienthoai_rong';
	}elseif(!preg_match('/^[0-9]*$/', $dienthoai)){
		$errors[] = 'dienthoai_sai';
	}elseif(!empty($taikhoan)){
		$errors[] = 'dienthoai_trung';
	}
	
	if(empty($matkhau1)) {
		$errors[] = 'matkhau_rong';
	}elseif(!preg_match('/^.{6,20}$/', $matkhau1)) {
		$errors[] = 'matkhau_sai';
	}
	
	if(empty($nhaplaimk)) {
		$errors[] = 'nhaplaimk_rong';
	}elseif(!preg_match('/^.{6,20}$/', $nhaplaimk)){
		$errors[] = 'nhaplaimk_sai';
	}
	if($matkhau1 != $nhaplaimk) {
		$errors[] = "matkhau_khac";
	}
	
	$smarty->assign('errors',$errors);
	
	if(empty($errors)) {
		$one = $lib->selectone("SELECT * FROM tbnhomtaikhoan WHERE nhomtaikhoan_macdinh=1 AND nhomtaikhoan_trangthai=1");
		$nhomtaikhoan = $one['nhomtaikhoan_id'];
		
		$strreg="INSERT INTO tbtaikhoan (taikhoan_phone, taikhoan_pass, taikhoan_user, taikhoan_hoten, taikhoan_email, taikhoan_ngaytao, taikhoan_trangthai, taikhoan_diachi, taikhoan_gioitinh, taikhoan_ngaysinh, truong_id, khoi_id, lop_id, nhomtaikhoan_id) VALUES (?, ?, ?, ?, ?, NOW(), 1, ?, ?, ?, ?, ?, ?, ?) ";
		$re=$lib->pdo->prepare($strreg);
		$re->bindParam(1,$dienthoai);
		$re->bindParam(2,$matkhau);
		$re->bindParam(3,$tendangnhap);
		$re->bindParam(4,$hoten);
		$re->bindParam(5,$email);
		$re->bindParam(6,$diachi);
		$re->bindParam(7,$gioitinh);
		$re->bindParam(8,$ngaysinh);
		$re->bindParam(9,$truong);
		$re->bindParam(10,$khoi);
		$re->bindParam(11,$lop);
		$re->bindParam(12,$nhomtaikhoan);
		$re->execute();
		$id = $lib->pdo->lastInsertId();
		
		$strtaikhoan="SELECT * FROM tbtaikhoan WHERE taikhoan_id='$id' ";
		$taikhoan=$lib->selectone($strtaikhoan);
		
		//Lưu thông tin đăng nhập vào SESSION
		$_SESSION['login']=$taikhoan;
		$_SESSION['login']['quyen']=array();
		
		$alert=array(true,'Đăng ký tài khoản thành công, mời bạn bấm vào <a href="/">đây</a> để quay về trang chủ.');
	}else{
		$alert=array(false,'Thông tin nhập vào không hợp lệ.');
	}
	if(isset($alert))$smarty->assign('alert',$alert);
}
$smarty->display('dangkytaikhoan.html');
?>