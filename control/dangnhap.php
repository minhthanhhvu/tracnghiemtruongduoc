<?php
	if(isset($_POST['btnlog'])){ //Khi người dùng bấm nút Đăng nhập
		$user=$_POST['txtuser'];
		$pass=md5($_POST['txtpass']);
		$strlog="SELECT * FROM tbtaikhoan WHERE taikhoan_user='$user' AND taikhoan_pass='$pass' AND taikhoan_trangthai=1 ";
		$log=$lib->selectone($strlog);
		if(!empty($log) && $log['nhomtaikhoan_id']>0){ //Nếu tên đăng nhập hoặc mật khẩu đúng
			//Lưu thông tin đăng nhập vào SESSION
			$_SESSION['login']=$log;
			
			//Thêm danh sách quyền vào SESSION
			$danhsach=$lib->selectall("SELECT quyen_ma FROM tbquyen WHERE quyen_trangthai=1 AND quyen_id IN (SELECT quyen_id FROM tbnhomtaikhoan_quyen WHERE nhomtaikhoan_id=".$_SESSION['login']['nhomtaikhoan_id'].") ",false);
			$quyen=array();
			foreach ($danhsach as $item){
				$quyen[]=$item['quyen_ma'];
			}
			$_SESSION['login']['quyen']=$quyen;
			
			if(isset($_POST['cbrem']) && $_POST['cbrem']==true){ //Nếu người dùng chọn Ghi nhớ mật khẩu
				setcookie('nhopass',$_SESSION['login']['taikhoan_id'],time()+2592000); //Lưu tài khoản trong vòng 1 tháng
			}
			
			//Cập nhật lại thời gian đăng nhập gần nhất
			$query="UPDATE tbtaikhoan SET taikhoan_truycap=NOW() WHERE taikhoan_id=?";
			$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
			$re->bindParam(1,$_SESSION['login']['taikhoan_id']);
			$re->execute();

			if(in_array('truycapquantri', $_SESSION['login']['quyen'])){
				$lib->redirect('quantri');
			}else{
				$kiemtra = $lib->selectone("SELECT * FROM tbkiemtra WHERE kiemtra_trangthai=1 AND kiemtra_batdau <= NOW() AND kiemtra_ketthuc >= NOW() AND kiemtra_id IN (SELECT DISTINCT kiemtra_id FROM tbkiemtra_phongthi WHERE phongthi_id = ".$_SESSION['login']['phongthi_id'].") ORDER BY kiemtra_batdau DESC");
				if(!empty($kiemtra)){
					$lib->redirect('index.php?pg=chitietbaikiemtra&id='.$kiemtra['kiemtra_id']);
				}
				else
					$lib->thongbao('Hiện tại bạn không có bài thi vào', 'index.php?pg=baikiemtra');
			}
		}else{ //Nếu tên đăng nhập hoặc mật khẩu sai
			$alert='Sai tên đăng nhập hoặc mật khẩu.';
			$smarty->assign('alert',$alert);
			
			$post=$_POST;
			$smarty->assign('post',$post);
			//$lib->thongbao('Sai tên đăng nhập hoặc mật khẩu','dang-nhap.html'); 
		}
	}
	$smarty->display('dangnhap.html');
?>