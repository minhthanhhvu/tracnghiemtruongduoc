<?php
if(isset($_COOKIE['nhopass'])){ //Nếu đã ghi nhớ mật khẩu
	//Lưu thông tin  đăng nhập vào SESSION
	$nhopass=$_COOKIE['nhopass'];
	$strlog="SELECT * FROM tbtaikhoan WHERE taikhoan_trangthai=1 AND taikhoan_id=".$nhopass;
	$_SESSION['login']=$lib->selectone($strlog);
	
	//Thêm danh sách quyền vào SESSION
	$danhsach=$lib->selectall("SELECT quyen_ma FROM tbquyen WHERE quyen_trangthai=1 AND quyen_id IN (SELECT quyen_id FROM tbnhomtaikhoan_quyen WHERE nhomtaikhoan_id=".$nhopass.") ",false);
	$quyen=array();
	foreach ($danhsach as $item){
		$quyen[]=$item['quyen_ma'];
	}
	$_SESSION['login']['quyen']=$quyen;
}

if (isset($_SESSION['login'])){ //Xử lý khi đăng nhập tài khoản thành công
	$strlog="SELECT * FROM tbtaikhoan WHERE taikhoan_id=".$_SESSION['login']['taikhoan_id']." AND taikhoan_trangthai=1 ";
	$log=$lib->selectone($strlog);
	$_SESSION['login'] = $log;

	//Thêm danh sách quyền vào SESSION
	$danhsach=$lib->selectall("SELECT quyen_ma FROM tbquyen WHERE quyen_trangthai=1 AND quyen_id IN (SELECT quyen_id FROM tbnhomtaikhoan_quyen WHERE nhomtaikhoan_id=".$_SESSION['login']['nhomtaikhoan_id'].") ",false);
	$quyen=array();
	foreach ($danhsach as $item){
		$quyen[]=$item['quyen_ma'];
	}
	$_SESSION['login']['quyen']=$quyen;

	//Xử lý logout
	if(isset($_GET['out']) && $_GET['out']==1){ //Nếu người dùng đăng xuất
		unset($_SESSION['login']);
		setcookie('nhopass','',time()-1);
		unset($_COOKIE['nhopass']);
		if(isset($_GET['url'])){
			$lib->redirect($_GET['url']);	
		}else{
			$lib->redirect('trang-chu.html');
		}
	}else{ //Nếu người dùng k có quyền truy cập quản trị
		//Truyền biến lưu thông tin tài khoản hiện tại ra ngoài tầng View 
		$login=$_SESSION['login'];
		$smarty->assign('login',$login);
	}
}

$counter=$lib->counter();//Gọi PT đếm lượt truy cập
$smarty->assign('counter',$counter);//TRuyền biến ra tầng view

//Lấy ra biến $pg
if(isset($_GET['pg']))$pg=$_GET['pg']; else $pg='baikiemtra';
$pg=str_replace('-','',$pg);
if(!file_exists('control/'.$pg.'.php')) $pg='baikiemtra';

//Lấy ra từ khóa tìm kiếm
if(isset($_GET['txtkey']))$key=$_GET['txtkey'];else $key='';
$smarty->assign('key',$key);//Khai báo biến

//Lấy ra danh sách bài kiểm tra đã thi, chưa thi và quá hạn
if(isset($_SESSION['login'])){
	$taikhoan_id=$_SESSION['login']['taikhoan_id'];
	$nhomtaikhoan_id=$_SESSION['login']['nhomtaikhoan_id'];
	
	$dathi=array();
	$thulai=array();
	$chuathi=array();
	$quahan=array();
	$quahanbatbuoc=array();
	$strkiemtra="SELECT * FROM tbkiemtra WHERE (kiemtra_id IN (SELECT kiemtra_id FROM tbnhomtaikhoankiemtra WHERE nhomtaikhoan_id=$nhomtaikhoan_id) OR kiemtra_id IN (SELECT kiemtra_id FROM tbtaikhoankiemtra WHERE taikhoan_id=$taikhoan_id)) ORDER BY kiemtra_ketthuc ASC";
	$kiemtra=$lib->selectall($strkiemtra,false); 
	foreach($kiemtra as $item){
		$one=$lib->selectone("SELECT * FROM tbkiemtra WHERE kiemtra_id=".$item['kiemtra_id']);		
		if($one['kiemtra_laphangngay']==1){ //Nếu bài kiểm tra lặp hàng ngày
			$ketquakiemtra=$lib->rowCount("SELECT 1 FROM tbketquakiemtra WHERE kiemtra_id=".$item['kiemtra_id']." AND taikhoan_id=".$taikhoan_id." AND DATE_FORMAT(ketquakiemtra_ngaytao,'%Y-%m-%d') = DATE_FORMAT(NOW(),'%Y-%m-%d') ");
		}else{ //Nếu bài kiểm tra k lặp hàng ngày
			$ketquakiemtra=$lib->rowCount("SELECT 1 FROM tbketquakiemtra WHERE kiemtra_id=".$item['kiemtra_id']." AND taikhoan_id=".$taikhoan_id);
		}
		
		if ($ketquakiemtra > 0){
			$dathi[]=$item['kiemtra_id'];
			if($ketquakiemtra < $one['kiemtra_thulai']){
				$thulai[]=$item['kiemtra_id'];
			}
		}else{
			if ($item['kiemtra_batdau'] <= date('Y-m-d H:i:s') && $item['kiemtra_ketthuc'] >= date('Y-m-d H:i:s')){ //Nếu bài kiểm tra có còn hạn
				$chuathi[]=$item['kiemtra_id'];	
			}else{
				$quahan[]=$item['kiemtra_id'];
				if($item['kiemtra_batbuoc']==1){ //Nếu là bài kiểm tra bắt buộc
					$quahanbatbuoc[]=$item['kiemtra_id'];
				}
			}
		}
	}
	$smarty->assign('dathi',$dathi);
	$smarty->assign('chuathi',$chuathi);
	$smarty->assign('quahan',$quahan);
	$smarty->assign('thulai',$thulai);
	$smarty->assign('quahanbatbuoc',$quahanbatbuoc);
}
$smarty->display('includes/header.html');
?>