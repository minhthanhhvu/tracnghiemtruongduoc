<?php
	if(isset($_GET['id'])){	
		$kiemtra_id=$_GET['id'];
		if(isset($_SESSION['idkiemtra']) && $_SESSION['idkiemtra']!=$kiemtra_id){
			unset($_SESSION['danhsach']);
			unset($_SESSION['ketquakiemtra']);
			unset($_SESSION['sttkiemtra']);
			unset($_SESSION['thoigiandatbau']);
		}
		$_SESSION['idkiemtra']=$kiemtra_id;

		//Lấy ra thông tin bài kiểm tra
		$detail=$lib->selectone("SELECT * FROM tbkiemtra WHERE kiemtra_id=$kiemtra_id");
		$smarty->assign('detail',$detail);
		
		//Cập nhật lại danh sách câu hỏi
		if(!isset($_SESSION['danhsach'])){
			//Lấy ra danh sách câu hỏi
			if($detail['kiemtra_tudongchon']==1){ //Nếu chọn câu hỏi tự động
				$cauhoitudong=$lib->selectall("SELECT * FROM tbcauhoitudong WHERE kiemtra_id=".$detail['kiemtra_id'],false);
				$strlay='';
				if(!empty($cauhoitudong)){
					foreach($cauhoitudong as $key=>$item){
						if($key>0)
							$strlay.=' UNION ';
						$strlay.=" (SELECT * FROM tbcauhoi WHERE cauhoi_trangthai=1 ";
						if(!empty($item['phongthi_id']))
							$strlay.=" AND phongthi_id=".$item['phongthi_id'];
						if(!empty($item['mucdo_id']))
							$strlay.=" AND mucdo_id=".$item['mucdo_id'];
						if(!empty($item['chude_id']))
							$strlay.=" AND chude_id=".$item['chude_id'];
						if(!empty($item['loaicauhoi_id']))
							$strlay.=" AND loaicauhoi_id=".$item['loaicauhoi_id'];
						if(!empty($item['doitac_id']))
							$strlay.=" AND doitac_id=".$item['doitac_id'];
						if(!empty($item['khuvuc_id']))
							$strlay.=" AND khuvuc_id=".$item['khuvuc_id'];
						if($detail['kiemtra_daocauhoi']==1)
							$strlay.=" ORDER BY RAND() ";
						$strlay.=" LIMIT ".$item['cauhoitudong_soluong'].") ";
					}
				}
			}else{ //Nếu chọn câu hỏi thủ công
				$strlay="SELECT * FROM tbcauhoi WHERE cauhoi_trangthai=1 AND cauhoi_id IN (SELECT cauhoi_id FROM tbkiemtra_cauhoi WHERE kiemtra_id=$kiemtra_id) ";
				if($detail['kiemtra_daocauhoi']==1)
					$strlay.=" ORDER BY RAND() ";
			}
			
			$danhsach=$lib->selectall($strlay,false);
			$_SESSION['danhsach']=$danhsach;
		}else{
			$danhsach=$_SESSION['danhsach'];
		}
		$smarty->assign('danhsach',$danhsach);

		//Truyền thời gian mỗi câu hỏi
		//$thoigianmoicau = (date('h', $detail['kiemtra_thoigian'])*60*60 + date('i', $detail['kiemtra_thoigian'])*60 + date('s', $detail['kiemtra_thoigian']))/count($danhsach);
		//$smarty->assign('thoigianmoicau',$thoigianmoicau);
		
		//Cập nhật lại thời gian kiểm tra
		if(!isset($_SESSION['thoigiandatbau'])){
			$_SESSION['thoigiandatbau']=date('H:i:s');
			$thoigianconlai=$detail['kiemtra_thoigian'];
		}else{
			$thoigianconlai=$lib->truthoigian($detail['kiemtra_thoigian'],($lib->truthoigian(date('H:i:s'),$_SESSION['thoigiandatbau'])));
		}
		
		//Lấy ra số lượng câu hỏi hiển thị trên 1 trang
		if(isset($_GET['lstkieuhienthi']))
			$kieuhienthi=$_GET['lstkieuhienthi'];
		else
			$kieuhienthi=$detail['kiemtra_kieuhienthi'];
			
		if(empty($kieuhienthi))
			$kieuhienthi=count($danhsach);
		
		if(!isset($_SESSION['sttkiemtra'])){
			$_SESSION['sttkiemtra']=0;
			$_SESSION['ketquakiemtra']=array();
		}
		
		if(!isset($_SESSION['ketquakiemtra']))
			$_SESSION['ketquakiemtra']=array();
		
		//Khi người dùng bấm Nộp bài
		if((isset($_POST['btnup']) && $_SESSION['sttkiemtra']==$_POST['sttkiemtra']) || (isset($_REQUEST['huy']) && $_REQUEST['huy']==1)){
			if(isset($_POST['cauhoi']))
				$cauhoi=$_POST['cauhoi'];
			else
				$cauhoi=array();

			if (isset($_REQUEST['huy']) && $_REQUEST['huy']){
			    $is_cancel = true;
            }else{
			    $is_cancel = false;
            }

			if(!$is_cancel){
				//Kiểm tra xem đã điền đầy đủ đáp án chưa?
				if (($_SESSION['sttkiemtra'] + $kieuhienthi) > count($danhsach))
					$tongso=count($danhsach);
				else
					$tongso=$_SESSION['sttkiemtra'] + $kieuhienthi;
				$chuatraloi=$tongso-$_SESSION['sttkiemtra']-count($cauhoi);
								
				//Tăng STT bài kiểm tra
				$_SESSION['sttkiemtra']+=$kieuhienthi;
				
				//Thêm kết quả vừa lấy được vào SESSION
				$_SESSION['ketquakiemtra']=$lib->ghep2mang($_SESSION['ketquakiemtra'],$cauhoi);
			}
				
			if ($_SESSION['sttkiemtra']>=count($danhsach) || $is_cancel){
				$cauhoi=$_SESSION['ketquakiemtra'];
				//$alert['diem']=0;
				$alert['caudung']=0;
				foreach($cauhoi as $key=>$item){
					$one=$lib->selectone("SELECT * FROM tbcauhoi WHERE cauhoi_trangthai=1 AND cauhoi_id=$key AND loaicauhoi_id IN (SELECT loaicauhoi_id FROM tbloaicauhoi WHERE loaicauhoi_trangthai=1 AND loaicauhoi_ma='tracnghiem')");
					if(!empty($one)){
						$count=$lib->rowCount("SELECT 1 FROM tbdapan WHERE dapan_dung=1 AND cauhoi_id=$key AND dapan_id=$item");
						if($count>0){
							$diemcong=$lib->selectone("SELECT * FROM tbmucdo WHERE mucdo_trangthai=1 AND mucdo_id=".$one['mucdo_id']);
							//$alert['diem']+=$diemcong['mucdo_diem'];
							$alert['caudung']++;
						}
					}
				}

				$alert['tongcauhoi']=count($danhsach);
				$alert['diem']=round((10/$alert['tongcauhoi'])*$alert['caudung']);
				$alert['tile']=round(($alert['caudung']/$alert['tongcauhoi'])*100,2);
				$xeploai=$lib->selectone("SELECT * FROM tbxeploai WHERE thangdiem_id=".$detail['thangdiem_id']." AND xeploai_thapnhat<=".$alert['tile']." AND xeploai_caonhat>=".$alert['tile']);
				$alert['xeploai']=$xeploai['xeploai_tieude'];
				$alert['ngaythi']=date('d/m/Y H:i');
				$taikhoan_id=$_SESSION['login']['taikhoan_id'];
				if(isset($_SESSION['thoigiandatbau'])){
					$alert['thoigian']=$lib->truthoigian(date('H:i:s'),$_SESSION['thoigiandatbau']);
				}else{
					$alert['thoigian']=0;
				}
				
				$query="INSERT INTO tbketquakiemtra (kiemtra_id, taikhoan_id, ketquakiemtra_thoigian, ketquakiemtra_ngaytao, ketquakiemtra_trangthai, ketquakiemtra_caudung, ketquakiemtra_tongcauhoi, ketquakiemtra_diem, xeploai_id) VALUES (?, ?, ?, NOW(), 1, ?, ?, ?, ?)";
				$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
				$re->bindParam(1,$kiemtra_id);
				$re->bindParam(2,$taikhoan_id);
				$re->bindParam(3,$alert['thoigian']);
				$re->bindParam(4,$alert['caudung']);
				$re->bindParam(5,$alert['tongcauhoi']);
				$re->bindParam(6,$alert['diem']);
				$re->bindParam(7,$xeploai['xeploai_id']);
				$re->execute();
				
				$ketquakiemtra_id=$lib->pdo->lastInsertId();
				$smarty->assign('ketquakiemtra_id',$ketquakiemtra_id);
				$ngaytao=date('Y-m-d H:m:s');
				foreach($danhsach as $key=>$item){
					$dapantuluan=NULL;
					$count=$lib->rowCount("SELECT * FROM tbcauhoi WHERE cauhoi_trangthai=1 AND cauhoi_id=$key AND loaicauhoi_id IN (SELECT loaicauhoi_id FROM tbloaicauhoi WHERE loaicauhoi_trangthai=1 AND loaicauhoi_ma='tuluan')");
					if($count>0){ //Nếu là câu hỏi tự luận
						$dapantuluan=$_SESSION['ketquakiemtra'][$item['cauhoi_id']];
						$dapan_id=NULL;
					}else{ //Nếu là câu hỏi trắc nghiệm
						if(array_key_exists($item['cauhoi_id'],$_SESSION['ketquakiemtra'])){
							$dapan_id=$_SESSION['ketquakiemtra'][$item['cauhoi_id']];
						}else{
							$dapan_id=NULL;
						}
					}
					
					$query="INSERT INTO tbketquakiemtra_cauhoi (ketquakiemtra_id, cauhoi_id, dapan_id, ngaytao, dapantuluan) VALUES (?, ?, ?, ?, ?)";
					$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
					$re->bindParam(1,$ketquakiemtra_id);
					$re->bindParam(2,$item['cauhoi_id']);
					$re->bindParam(3,$dapan_id);
					$re->bindParam(4,$ngaytao);
					$re->bindParam(5,$dapantuluan);
					$re->execute();
				}
				
				//$sttkiemtra = $_SESSION['sttkiemtra']-1;
				//Hủy biến SESSION
				unset($_SESSION['idkiemtra']);
				unset($_SESSION['danhsach']);
				unset($_SESSION['ketquakiemtra']);
				unset($_SESSION['sttkiemtra']);
				unset($_SESSION['thoigiandatbau']);
				
				$lib->thongbao("Kết thúc bài thi. Vui lòng bấm OK để xem lại kết quả.", "index.php?pg=ketquakiemtra&kiemtra_id=".$detail['kiemtra_id']."&ketquakiemtra_id=$ketquakiemtra_id");
			}
		}
		
		if(isset($_SESSION['sttkiemtra']))
			$smarty->assign('sttkiemtra',$_SESSION['sttkiemtra']);
		$smarty->assign('kieuhienthi',$kieuhienthi);
		$smarty->assign('thoigianconlai',$thoigianconlai);
		$smarty->display('kiemtra.html');
	}
?>