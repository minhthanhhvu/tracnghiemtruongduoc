<?php
if(isset($_SESSION['login']['taikhoan_id'])){
	$id=$_SESSION['login']['taikhoan_id'];
	if(isset($_POST['btnup'])){
		if($_POST['txtoldpass']==$_POST['txtpass'])
			$pass=$_POST['txtpass'];
		else
			$pass=md5($_POST['txtpass']);
		$email=$_POST['txtemail'];
		$hoten=$_POST['txthoten'];
		$gioitinh=$_POST['lstgioitinh'];
		$ngaysinh=$lib->date_convert($_POST['txtngaysinh']);
		$diachi=$_POST['txtdiachi'];
		$phone=$_POST['txtphone'];
		if(isset($_POST['lsttruong']))
			$truong=$_POST['lsttruong'];
		else
			$truong=NULL;
		if(isset($_POST['lstkhoi']))
			$khoi=$_POST['lstkhoi'];
		else
			$khoi=NULL;
		if(isset($_POST['lstlop']))
			$lop=$_POST['lstlop'];
		else
			$lop=NULL;
		$sobaodanh = $_POST['txtsobaodanh'];
		$lopchungchi = $_POST['txtlopchungchi'];
		
		//Xử lý cập nhật dữ liệu trên vào CSDL luôn
		$query="UPDATE tbtaikhoan SET taikhoan_email=?, taikhoan_pass=?, taikhoan_hoten=?, taikhoan_gioitinh=?, taikhoan_ngaysinh=?, taikhoan_diachi=?, taikhoan_phone=?, taikhoan_ngaysua=NOW(), truong_id=?, khoi_id=?, lop_id=?, taikhoan_sobaodanh=?, taikhoan_lopchungchi=? WHERE taikhoan_id=?";
		$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
		$re->bindParam(1,$email);
		$re->bindParam(2,$pass);
		$re->bindParam(3,$hoten);
		$re->bindParam(4,$gioitinh);
		$re->bindParam(5,$ngaysinh);
		$re->bindParam(6,$diachi);
		$re->bindParam(7,$phone);
		$re->bindParam(8,$truong);
		$re->bindParam(9,$khoi);
		$re->bindParam(10,$lop);
		$re->bindParam(11,$sobaodanh);
		$re->bindParam(12,$lopchungchi);
		$re->bindParam(13,$id);
		$re->execute();
		$lib->redirect('/');
	}

	$detail=$lib->selectone("SELECT * FROM tbtaikhoan WHERE taikhoan_id=$id");
	$smarty->assign('detail',$detail);//Khai báo biến tầng View
	$smarty->display('thongtincanhan.html');
}else{
	$lib->redirect('/');
}