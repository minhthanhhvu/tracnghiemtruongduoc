<?php
//Khai báo đối tượng LIB
class LIB
{
	//Thuộc tính pdo: chứa tham chiếu đối tượng PDO khi khởi tạo, muốn sử dụng đối tượng này bất cứ đâu trong chương trình (public)
	public $pdo;
	//Phương thức construct: tự động chạy khi khởi tạo đối tượng: chứa lệnh khởi tạo PDO để kết nối đến CSDL
	//Đối số: Truyền vào: mảng chưa thông tin kết nối CSDL
	public function __construct($cf)
	{
		//try catch: lệnh bắt lỗi khi xảy ra
		try{
			//Code mặc định sẽ chạy trong try: nếu lỗi thì catch đc chạy thay thế
			//Khởi tạo đối tượng PDO và tham chiếu lên thuộc tính pdo
			$dsn=$cf['server'].':dbname='.$cf['db'].';host='.$cf['host'].';charset=utf8';
			$this->pdo= new PDO($dsn,$cf['user'],$cf['pass']);			
		}catch(PDOException $ex){
			//Dừng lại chương trình và in ra lỗi
			die($ex->getMessage());
		}
	}
	//Các thuộc tính phục vụ phân trang
	public $p;//Số phần tử trên 1 trang: Truyền từ bên ngoài vào
	private $num,$n;
	//$num: Tổng số trang tính đc dựa vào số dòng dữ liệu và số phần tử trên 1 trang
	//$n: Tranh hiện tại mà người dùng đang đc xem
	
	//Phương thức: Selectall: Chạy câu lệnh truy vấn và trả về mảng 2 chiều
	//ĐỐi số: 1: câu lệnh SQL, 2: Biến chỉ định trạng thái có phân trang hay ko (mặc định có)
	//Giá trị trả về: mảng 2 chiều chứa toàn bộ dữ liệu trả về bởi câu lệnh SQL
	public function selectall($query,$pt=true)
	{
		$re=$this->pdo->query($query);//Chạy câu lệnh SQL
		if(!$re){
			echo 'ERROR: ';
			print_r($this->pdo->errorInfo());
			die('<br>SQL:'.$query);
		}
		if($pt){//Kiểm tra xem có phân trang hay ko
			$this->num=ceil($re->rowCount()/$this->p);//Tính số trang và gán cho thuộc tính num (lệnh ceil : để làm tròn 1 số)
			if(isset($_GET['n']))$this->n=$_GET['n'];else $this->n=1;//Lấy trang hiện tại trên URL
			if($this->n>$this->num)$this->n=$this->num;//Nếu trang đang xem vượt quá số trang thì...
			if($this->n<1)$this->n=1;//Nếu trang đang xem nhỏ hơn trang 1 thì gán trở về trang 1
			$thutu=($this->n-1)*$this->p;//Công thức tính thứ tự bản ghi sẽ lấy
			$query.=" LIMIT $thutu , ".$this->p;//Thay đổi câu lệnh SQL, nối thêm lệnh LIMIT (giới hạn)
			$re=$this->pdo->query($query);//Chạy câu lệnh SQL
			if(!$re)die('ERROR:'.$query);//Kiểm tra câu lệnh SQL
		}
		return $re->fetchAll();//PT trả về giá trị mảng 2 chiều	
	}
	//Phương thức viewpage: Hiển thị phân trang theo thuộc tính $num đã tính đc
	//ĐỐi số: Link phân trang
	public function viewpage($link)
	{
		//Tính điểm bắt đầu và kết thúc của vòng lặp in phân trang
		$begin=$this->n-4;
		$end=$this->n+4;
		if($begin<1){
			$begin=1;
			$end=$begin+8;
		}
		if($end>$this->num){
			$end=$this->num;
			$begin=$end-8;
		}
		if($begin<1)$begin=1;
		if($this->n>5) echo '<a href="'.$link.'1" >Trang đầu</a> ';//In ra link mũi trên trái ( bấm lên trang  đầu tiên)
		//Vòng lặp để in ra phần số trang
		for($i=$begin;$i<=$end;$i++)
		{
			if($i==$this->n)$ac='active';else $ac='';//Điều kiện lấy giá trị active cho style sheet
			echo '<a href="'.$link.$i.'" class="'.$ac.'" >'.$i.'</a> ';//In ra số trang
		}	
		if($this->n<$this->num - 4) echo '<a href="'.$link.$this->num.'" >Trang cuối</a> ';//In ra link mũi trên phải (bấm xuống trang cuối)
	}
	//Phương thức selectone: lấy ra 1 dòng dữ liệu đầu tiên bởi câu SQL
	//ĐỐi số: câu lệnh SQL
	//Giá trị trả về: Mảng 1 chiều chứa thông tin của 1 dòng dữ liệu
	public function selectone($query)
	{
		$re=$this->pdo->query($query);//Chạy câu lệnh SQL
		if(!$re)die('ERROR:'.$query);//Kiểm tra câu lệnh
		return $re->fetch();//PT trả về dữ liệu mảng 1 chiều chứa thông tin 1 dòng dữ liệu có đc
	}
	
	public function rowCount($query)
	{
		$re=$this->pdo->query($query);//Chạy câu lệnh SQL
		if(!$re)die('ERROR:'.$query);//Kiểm tra câu lệnh
		return $re->rowCount();//PT trả về dữ liệu mảng 1 chiều chứa thông tin 1 dòng dữ liệu có đc
	}
	
	//Phương thức uploadfile: Upload 1 file từ form file lên website 
	//2 Đối số : 1: tên của form file upload, 2: thư mục, đường dẫn sẽ lưu file này
	//Giá trị trả về: trả về đường dẫn của file nếu upload thành công, hoặc FALSE
	public function uploadfile($nameform,$dir='../upload/')
	{
		if(isset($_FILES[$nameform]))
		{//Kiểm tra xem có file upload không
			$name=$_FILES[$nameform]['name'];//Lấy ra tên file
			$size=$_FILES[$nameform]['size'];//Lấy ra dung lượng file
			$tmp=$_FILES[$nameform]['tmp_name'];//Lấy ra đường dẫn tạm của file
			$error=$_FILES[$nameform]['error'];//Lấy ra mã lỗi của file
			//Lấy đuôi mở rộng của file
			$arr=explode('.',$name);//Tách chuỗi thành 1 mảng dựa vào dấu chấm
			$ext=strtolower($arr[count($arr)-1]);//Lấy ra phần tử cuối cùng của mảng
			//Sử dụng biểu thức điều kiện kiểm tra đuôi mở rộng này
			if($ext=='xls' || $ext=='xlsx')
			{
				$des=$dir.$name;//Đường dẫn File sẽ đc di chuyển đến
				if(move_uploaded_file($tmp,$des))return $des;
				else return false;
			}else return false;			
		}else return false;		
	}
	
	//Phương thức manacart:  Quản lý giỏ hàng mà người dùng đặt mua
	//Đối số: ID sản phẩm, số lượng sản phẩm sẽ mua, kiểu update số lượng=true(tăng số lượng)
	public function manacart($id,$sl,$type=true)
	{
		if(!isset($_SESSION['giohang']))$_SESSION['giohang']=array();//Nếu giỏ hàng chưa tồn tại thì khởi tạo
		$tim=false;//Biến lưu trạng thái tìm kiếm sản phẩm trong giỏ hàng
		for($i=0;$i<count($_SESSION['giohang']);$i++)//Sử dụng vòng lặp để lấy ra từng sản phẩm trong giỏ hàng
		{//Mỗi vòng lặp sẽ lấy đc thông tin của 1 sản phẩm trong biến giỏ hàng: gán giá trị cho biến $sp
			$sp=$_SESSION['giohang'][$i];//Thông tin của 1 sản phẩm trong giỏ hàng
			if($sp[0]==$id){//Nếu sản phẩm có trong giỏ hàng rồi thì...
				$tim=true;//Thay đổi trạng thái tìm kiếm thành true (có thấy)
				if($sl<1 || !is_numeric($sl)){//Nếu số lượng mua nhỏ hơn 0 hoặc ko là số thì xóa sản phẩm
					unset($_SESSION['giohang'][$i]);//Xóa sản phẩm trong mảng giỏ hàng
					$_SESSION['giohang']=array_values($_SESSION['giohang']);//Sắp xếp lại phần tử trong giỏ hàng
				}elseif($type){//Update cộng thêm số lượng với số lượng cũ
					$slmoi=$sl+$sp[1];//Cộng số lượng thêm với số lượng cũ để lấy số lượng mới sẽ mua
					$_SESSION['giohang'][$i]=array($id,$slmoi);//Update lại số lượng cho sản phẩm trong giỏ hàng
				}else 
				$_SESSION['giohang'][$i]=array($id,$sl);//Thay thế số lượng cũ bằng số lượng mới cho sản phẩm				
			}
		}
		if(!$tim)array_push($_SESSION['giohang'],array($id,$sl));//Thêm sản phẩm vào giỏ hàng
	}
	
	
	public function maketitle($text)
	{
		$text = html_entity_decode($text);
		$text=trim($text);//Loại bỏ ký tự trắng đầu và cuối
		//Loại bỏ utf8
		$text = preg_replace("/(ä|à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $text);
		$text = str_replace("ç","c",$text);
		$text = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $text);
		$text = preg_replace("/(ì|í|î|ị|ỉ|ĩ)/", 'i', $text);
		$text = preg_replace("/(ö|ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $text);
		$text = preg_replace("/(ü|ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $text);
		$text = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $text);
		$text = preg_replace("/(đ)/", 'd', $text);
		//CHU HOA
		$text = preg_replace("/(Ä|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $text);
		$text = str_replace("Ç","C",$text);
		$text = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $text);
		$text = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $text);
		$text = preg_replace("/(Ö|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $text);
		$text = preg_replace("/(Ü|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $text);
		$text = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $text);
		$text = preg_replace("/(Đ)/", 'D', $text);
		//Ký tự đặc biệt
		$text = str_replace(" / ","-",$text);
		$text = str_replace("/","-",$text);
		$text = str_replace(" - ","-",$text);
		$text = str_replace("_","-",$text);
		$text = str_replace( "ß", "ss", $text);
		$text = str_replace(" ",'-',$text);//chuyển dấu cách ->gạch (-)
		//Chạy lệnh cuối: Loại bỏ các ký hiệu đặc biệt còn lại
		$text=preg_replace("/[^-a-zA-Z0-9]/",'',$text);
		$text = str_replace("--",'-',$text);
		return trim(strtolower($text));
	}
	
	//Phương thức sendmail: Xử lý gửi email cho người dùng
	//4 đối số: 1:email người nhận, 2 tên người nhận, 3 tiêu đề bức thư, 4 nội dung bức thư
	//Giá trị trả về: Lỗi khi gửi mail (nếu có)
	public function sendmail($to,$name,$tieude,$noidung)
	{
		require('class.phpmailer.php');
		//Khởi tạo đối tượng PHPMAILER
		$mail= new PHPMailer;
		//Chia làm 2 phần cấu hình cho nghiệp vụ gửi mail này: 1 cấu hình server trung gian, 2 cấu hình nội dung mail
		//1. Cấu hình server trung gian
		$mail->IsSMTP();//Khai báo phương thức gửi mail SMTP 
		$mail->Host='smtp.gmail.com';//Khai báo tên server trung gian
		$mail->Port='465';//Khai báo cổng chạy dịch vụ mail
		$mail->SMTPAuth=true;//Bật chế độ xác nhận tài khoản
		$mail->Username='huynguyen2705@gmail.com';//Tài khoản mail trên gmail
		$mail->Password='Troidatoi1';//Mật khẩu tương ứng
		$mail->SMTPSecure='ssl';
		//2. Cấu hình nội dung của mail
		$mail->AddAddress($to,$name);//Thêm địa chỉ của người nhận thư điện tử
		$mail->FromName='Buyplus';//Tên người gửi thay thể cho chuỗi Root user
		$mail->AddReplyTo('namng@itplus.edu.vn','NAM NG');//Thêm địa chỉ của Người nhận trả lời
		$mail->IsHTML(true);//Cấu hình cho phép chạy HTML
		$mail->CharSet='utf8';//Cho phép hiển thị tiếng việt
		$mail->Subject=$tieude;//Tiêu đề thư
		$mail->Body=$noidung;//Nội dung thư
		$mail->Send();//Gọi Phương thức gửi mail
		return $mail->ErrorInfo;//Thuộc tính lưu trữ lỗi (nếu có) xảy ra khi gửi mail
		
	}
	//Phương thức loadmenu: Phương thức đệ quy đổ ra menu đa cấp từ bảng tbdanhmuc
	//1 Đối số: id của danh mục cha: $parent (mặc định giá trị 0): danh mục gốc
	public function loadmenu($parent=0,$vitri=1)
	{
		$strlay="SELECT * FROM tbdanhmuc WHERE danhmuc_trangthai=1 AND danhmuc_parent='$parent' AND danhmuc_id IN (SELECT danhmuc_id FROM tbdanhmuc_vitri WHERE vitri_id=$vitri) ";//Xây dựng câu lệnh truy vấn SQL
		$re=$this->pdo->query($strlay);//Chạy câu lệnh SQL
		if(!$re)die('Error Query:'.$strlay);//Kiểm tra lỗi (nếu có) in ra
		if($re->rowCount()>0){//Nếu có dữ liệu lấy đc thì...
			echo "<ul>";
			//Sử dụng vòng lặp để lấy ra từng danh mục 
			while($row=$re->fetch())
			{//Mỗi vòng lặp biến $row là 1 danh mục
				echo '<li>';
				echo '<a href="category-'.$this->maketitle($row['danhmuc_tieude']).'-'.$row['danhmuc_id'].'.html"  class="title">'.$row['danhmuc_tieude'].'</a>';				
				$this->loadmenu($row['danhmuc_id'],$vitri);//Gọi hàm đệ quy
				echo '</li>';
			}
			echo '</ul>';
		}
	}
	//PT counter: đếm lượt truy cập và đang online
	//Giá trị trả về là mảng chứa 2 thông tin. lượt truy cập, đang online
	public function counter()
	{
		$ip=$_SERVER['REMOTE_ADDR'];//Lấy địa chỉ IP
		$time=time();//Lấy về thời gian hiện tại trên hệ thống (tính bằng giây)
		//Nếu lần đâu vào website
		if(!isset($_SESSION['counter'])){
			$_SESSION['counter']=md5($time.$ip.rand());//Tạo ra giá trị cho biến session
			//Thêm dữ liệu vào bảng tbcounter
			$stradd="INSERT INTO tbcounter (counter_session, counter_ip, counter_time) VALUES (?,?,?)";
			$re=$this->pdo->prepare($stradd);//Đưa câu lệnh vào trạng thái chuẩn bị
			//Truyền các tham số cho câu lệnh trên
			$re->bindParam(1,$_SESSION['counter']);
			$re->bindParam(2,$ip);
			$re->bindParam(3,$time);
			$re->execute();//Chạy câu lệnh trên
		}else{
			$stredit="UPDATE tbcounter counter_time=? WHERE counter_session=?";
			$re=$this->pdo->prepare($stredit);//Đưa câu lệnh vào trạng thái chuẩn bị
			$re->bindParam(1,$time);
			$re->bindParam(2,$_SESSION['counter']);
			$re->execute();//Chạy câu lệnh trên
		}
		//Lấy ra Số lượt truy cập
		$re=$this->pdo->query("SELECT * FROM tbcounter");
		$tong=$re->rowCount();//Lấy tổng số bản ghi trong re
		//Lấy ra Số người đang online
		$min=$time-30*60;
		$re=$this->pdo->query("SELECT * FROM tbcounter WHERE  counter_time>=$min AND counter_time<=$time");
		$online=$re->rowCount();//Lấy tổng số bản ghi trong re
		return array('tong'=>$tong,'online'=>$online);//Giá trị trả về
	}
	//PT viewpro: PT đếm lượt xem sản phẩm
	//Đối số: Mã sản phẩm (ID)
	public function viewpro($id)
	{
		if(!isset($_SESSION['viewpro']))$_SESSION['viewpro']=array();
		//Kiểm tra ID truyền vào xem có trong danh sách sản phẩm đã xem hay ko
		$find=false;
		foreach ($_SESSION['viewpro']as $item)
		{
			if($item==$id){
				$find=true;
				return false;
			}
		}
		//Nếu ko có ID sản phẩm trong danh sách đã xem thì: cập nhật lượt xem thêm 1 đơn vị
		if(!$find){
			$this->pdo->exec("UPDATE tbsanpham SET sanpham_view=sanpham_view+1 WHERE sanpham_id=$id");	
			$_SESSION['viewpro'][]=$id;//Bổ xung ID sản phẩm vào danh sách đã xem
			return true;
		}
	}
	
	public function genhd($year)
	{
		$tk=rand(1,9);
		$tieude='Hóa đơn của '.$tk;
		$giatri=rand(1,10)*1000000;
		$time=$year.'-'.rand(1,12).'-'.rand(1,28).' '.rand(0,23).':'.rand(0,59).':30';
		$tt=5;
		$stradd="INSERT INTO tbhoadon (taikhoan_id,	hoadon_tieude, hoadon_giatri, hoadon_date, hoadon_trangthai) VALUES (?,?,?,?,?)";
		$re=$this->pdo->prepare($stradd);//Đưa câu lệnh vào trạng thái chuẩn bị
		//Truyền các tham số cho câu lệnh trên
		$re->bindParam(1,$tk);
		$re->bindParam(2,$tieude);
		$re->bindParam(3,$giatri);
		$re->bindParam(4,$time);
		$re->bindParam(5,$tt);
		$re->execute();//Chạy câu lệnh trên
	}
	
	//PT getdoanhso: Lấy về tổng số tiền trong 1 khoảng thời gian
	//2 đối số: Thời gian min, thời gian max
	//Giá trị trả về: Tổng tiền
	public function getdoanhso($min,$max)
	{
		$strlay="SELECT SUM(hoadon_giatri) AS giatri FROM tbhoadon WHERE hoadon_trangthai=5 AND hoadon_date>='$min' AND hoadon_date<'$max'";
		$re=$this->pdo->query($strlay);
		$row=$re->fetch();
		return $row['giatri']/1000000;
	}
	
	public function getyear($month,$year='')
	{
		if($year=='')$year=date('Y');//Nếu ko xác định năm thì mặc định là năm hiện tại
		$min=$year.'-'.$month.'-01 00:00:00';
		$next=$month+1;
		$max=$year.'-'.$next.'-01 00:00:00';
		return $this->getdoanhso($min,$max);
	}
	
	
	public function loadthongso($nhom_id, $sanpham_id=0){
		$listthongso=$this->selectall("SELECT * FROM tbthongso WHERE nhom_id={$nhom_id} AND thongso_trangthai=1 ORDER BY thongso_thutu, thongso_id",false);
		foreach ($listthongso as $item){
			$listchitiet=$this->selectall("SELECT * FROM tbchitietthongso WHERE thongso_id=".$item['thongso_id'],false);
			
			echo '<tr>';
			echo '<td>'.$item['thongso_tieude'].'</td>';
			echo '<td>';
			switch($item['thongso_control']){
				case 0:
						echo '<input type="text" name="tskhac'.$item['thongso_id'].'">';
						break;
				case 1:
						echo '<select name="ts'.$item['thongso_id'].'">';
						echo '<option value="">Vui lòng chọn</option>';
						foreach ($listchitiet as $chitiet){
							echo '<option value="'.$chitiet['chitiet_id'].'">'.$chitiet['chitiet_tieude'].'</option>';
						}
						echo '</select>';
						echo 'Hoặc điền <input name="tskhac'.$item['thongso_id'].'" type="text">';
						break;
				case 2:
						foreach ($listchitiet as $chitiet){
							echo '<input name="ts'.$item['thongso_id'].'[]" type="checkbox">'.$chitiet['chitiet_tieude'].'<br>';
						}
						break;
			}
		}
	}
	
	public function redirect($link){
		header('Location: '.$link);
	}
	
	public function loadquyen($parent=0, $selected='')
	{
		$strlay="SELECT * FROM tbquyen WHERE quyen_trangthai=1 AND quyen_parent='$parent' ORDER BY quyen_id ASC";//Xây dựng câu lệnh truy vấn SQL
		$re=$this->pdo->query($strlay);//Chạy câu lệnh SQL
		if(!$re)die('Error Query:'.$strlay);//Kiểm tra lỗi (nếu có) in ra
		if($re->rowCount()>0){//Nếu có dữ liệu lấy đc thì...
			echo "<ul>";
			//Sử dụng vòng lặp để lấy ra từng danh mục 
			while($row=$re->fetch())
			{//Mỗi vòng lặp biến $row là 1 danh mục
				$str="SELECT 1 FROM tbnhomtaikhoan_quyen WHERE nhomtaikhoan_id=$selected AND quyen_id=".$row['quyen_id'];
				if ($selected!='' && $this->rowCount($str)>0)
					$checked='checked';
				else
					$checked='';					
				echo '<li>';
				
				$str="SELECT 1 FROM tbquyen WHERE quyen_parent=".$row['quyen_id']." AND quyen_trangthai=1";
				if ($this->rowCount($str)==0)
					echo '<input type="checkbox" name="quyen[]" value="'.$row['quyen_id'].'" '.$checked.'>';
				echo $row['quyen_tieude'];				
				$this->loadquyen($row['quyen_id'],$selected);//Gọi hàm đệ quy
				echo '</li>';
			}
			echo '</ul>';
		}
	}
	
	public function subtitle($text, $total = 30) {
		$text=strip_tags($text);
		$text =  preg_replace('/\s(?=\s)/', '', trim($text));
        if(strlen($text) > $total) {
            $cutString = substr($text,0,$total);
            $words = substr($text, 0, strrpos($cutString, ' '));
            $text=$words;
			return $text.'...';
        }
        return $text;
  	}
	
	//Lấy đường dẫn hiện tại trên url
		public function getsiteurl() {
			$pageURL = 'http';
			if (!empty($_SERVER['HTTPS'])) {if($_SERVER['HTTPS'] == 'on'){$pageURL .= "s";}}
			$pageURL .= "://";
			if ($_SERVER["SERVER_PORT"] != "80") {
				$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
			} else {
				$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
			}
			return $pageURL;
		}
	
	public function thongbao($str='',$url='')
	{
		echo '<script type="text/javascript">';
		if($str!='') echo 'alert("'.$str.'");';
		if($url!='') echo 'window.location.href="'.$url.'";';
		echo '</script>';
	}
	
	public function messagebox($str='')
	{
		echo '
			<script type="text/javascript">
				var r = confirm("'.$str.'");
				if (r == false) {					
					var xemlai=1;
				}
			</script>
		';
	}
	
	public function loadmenu_combobox($parent=0, $selected='')
	{
		$strlay="SELECT * FROM tbdanhmuc WHERE danhmuc_trangthai=1 AND danhmuc_parent='$parent' ORDER BY danhmuc_thutu ASC";//Xây dựng câu lệnh truy vấn SQL
		$re=$this->pdo->query($strlay);//Chạy câu lệnh SQL
		if(!$re)die('Error Query:'.$strlay);//Kiểm tra lỗi (nếu có) in ra
		if($re->rowCount()>0){//Nếu có dữ liệu lấy đc thì...
			//Sử dụng vòng lặp để lấy ra từng danh mục 
			while($row=$re->fetch())
			{//Mỗi vòng lặp biến $row là 1 danh mục
				if ($row['danhmuc_parent']!=0) $level='--'; else $level='';
				if ($selected!='' && $row['danhmuc_id']==$selected) $active='selected'; else $active='';
				echo '<option value="'.$row['danhmuc_id'].'" '.$active.'>'.$level.$row['danhmuc_tieude'].'</option>';				
				$this->loadmenu_combobox($row['danhmuc_id'],$selected);//Gọi hàm đệ quy
			}
		}
	}
	
	public function loadmenu_admin($parent=0,$vitri=1)
	{
		$strlay="SELECT * FROM tbdanhmuc WHERE danhmuc_trangthai=1 AND danhmuc_parent='$parent' AND danhmuc_id IN (SELECT danhmuc_id FROM tbdanhmuc_vitri WHERE vitri_id=$vitri) ORDER BY danhmuc_thutu ASC";//Xây dựng câu lệnh truy vấn SQL
		$re=$this->pdo->query($strlay);//Chạy câu lệnh SQL
		if(!$re)die('Error Query:'.$strlay);//Kiểm tra lỗi (nếu có) in ra
		if($re->rowCount()>0){//Nếu có dữ liệu lấy đc thì...
			echo "<ul>";
			//Sử dụng vòng lặp để lấy ra từng danh mục
			while($row=$re->fetch())
			{//Mỗi vòng lặp biến $row là 1 danh mục
				$nhomdanhmuc=$this->selectone("SELECT nhomdanhmuc_ma FROM tbnhomdanhmuc WHERE nhomdanhmuc_trangthai=2 AND nhomdanhmuc_id=".$row['nhomdanhmuc_id']);
				$arr=explode('_',$nhomdanhmuc['nhomdanhmuc_ma']);
				$link="index.php";
				if(isset($arr[0])){
					$view=$arr[0];
					$link.="?view=$view";
				}else{
					$view='';
				}
				if(isset($arr[1])){
					$act=$arr[1];
					$link.="&act=$act";
				}else 
					$act='';
				if (isset($_GET['view']) && $_GET['view']==$view && $row['danhmuc_parent']==0)
					$active='class="active"';
				else
					$active='';
				echo '<li '.$active.'>';
				if($view=='truycapnguoidung'){
					echo '<a href="/">'.$row['danhmuc_tieude'].'</a>';
				}else{
					//Kiểm tra quyền để hiển thị
					$checkquyen=false;
					foreach($_SESSION['login']['quyen'] as $quyen){
						$q=explode('_',$quyen);
						if($q[0]==$view)
							$checkquyen=true;
					}
					if($checkquyen){
						echo '<a href="'.$link.'">'.$row['danhmuc_tieude'].'</a>';
					}
				}
				$this->loadmenu_admin($row['danhmuc_id'],$vitri);//Gọi hàm đệ quy
				echo '</li>';
			}
			echo '</ul>';
		}
	}
	
	public function date_convert($date,$format='yyyy-mm-dd'){
		$date=trim($date);
		$date=str_replace(' ','',$date);
		switch($format){
			case 'yyyy-mm-dd': 
						$ngay=substr($date,0,2);
						$thang=substr($date,3,2);
						$nam=substr($date,6,4);
						$result=$nam.'-'.$thang.'-'.$ngay;
						break;
			case 'yyyy-mm-dd hh:ii:ss':
						$ngay=substr($date,0,2);
						$thang=substr($date,3,2);
						$nam=substr($date,6,4);
						$time=substr($date,10,5);
						$result=$nam.'-'.$thang.'-'.$ngay.' '.$time;
						break;
			case 'hh:ii:ss':
						$gio=substr($date,0,2);
						$phut=substr($date,3,2);
						$giay=substr($date,6,2);
						$result=$gio.':'.$phut.':'.$giay;
						break;
			
		}
		if (isset($result))
			return $result;
		else
			return false;
	}
	
	function truthoigian($hour_one,$hour_two){
		//$hour_one = "01:20:20";
		//$hour_two = "05:50:20";
		$h =  strtotime($hour_one);
		$h2 = strtotime($hour_two);
	 
		$minute = date("i", $h2);
		$second = date("s", $h2);
		$hour = date("H", $h2);
	 
		$convert = strtotime("-$minute minutes", $h);
		$convert = strtotime("-$second seconds", $convert);
		$convert = strtotime("-$hour hours", $convert);
		$new_time = date('H:i:s', $convert);
	 
		return $new_time;
	}
	
	function ghep2mang($a1, $a2) {
		$aRes = $a1;
		foreach ( array_slice ( func_get_args (), 1 ) as $aRay ) {
			foreach ( array_intersect_key ( $aRay, $aRes ) as $key => $val )
				$aRes [$key] += $val;
			$aRes += $aRay;
		}
		return $aRes;
	}
	
	/*
	function merge($arr1,$arr2)
	{
		if(!is_array($arr1))
			$arr1 = array();
		if(!is_array($arr2))
			$arr2 = array();
		$keys1 = array_keys($arr1);
		$keys2 = array_keys($arr2);
		$keys  = array_merge($keys1,$keys2);
		$vals1 = array_values($arr1);
		$vals2 = array_values($arr2);
		$vals  = array_merge($vals1,$vals2);
		$ret    = array();
	
		foreach($keys as $key)
		{
			list($unused,$val) = each($vals);
			$ret[$key] = $val;
		}
	
		return $ret;
	}
	*/
	
	function delete($table,$id=''){
		switch($table){
			case 'tbcauhoi':
							if($id==''){
								$danhsach=$this->selectall("SELECT cauhoi_id FROM tbcauhoi",false);
								foreach($danhsach as $item)
									$this->delete('tbcauhoi',$item['cauhoi_id']);
							}else{
								$this->pdo->exec("DELETE FROM tbdapan WHERE cauhoi_id=$id"); //Xóa đáp án						
								$this->pdo->exec("DELETE FROM tbkiemtra_cauhoi WHERE cauhoi_id=$id"); //Xóa câu hỏi kiểm tra			
								$this->pdo->exec("DELETE FROM tbketquakiemtra_cauhoi WHERE cauhoi_id=$id"); //Xóa câu hỏi trong kết quả kiểm tra
								$this->pdo->exec("DELETE FROM tbcauhoi WHERE cauhoi_id=$id"); //Xóa câu hỏi
							}
							break;
			case 'tbketquakiemtra':
							if($id==''){
								$danhsach=$this->selectall("SELECT ketquakiemtra_id FROM tbketquakiemtra",false);
								foreach($danhsach as $item)
									$this->delete('tbketquakiemtra',$item['ketquakiemtra_id']);
							}else{	
								$this->pdo->exec("DELETE FROM tbketquakiemtra_cauhoi WHERE ketquakiemtra_id=$id"); //Xóa câu hỏi của kết quả kiểm tra
								$this->pdo->exec("DELETE FROM tbketquakiemtra WHERE ketquakiemtra_id=$id"); //Xóa kết quả kiểm tra
							}
							break;
			case 'tbkiemtra':
							if($id==''){
								$danhsach=$this->selectall("SELECT kiemtra_id FROM tbkiemtra",false);
								foreach($danhsach as $item)
									$this->delete('tbkiemtra',$item['kiemtra_id']);
							}else{
								$danhsach=$this->selectall("SELECT ketquakiemtra_id FROM tbketquakiemtra WHERE kiemtra_id=$id",false);
								foreach ($danhsach as $item)
									$this->delete('tbketquakiemtra',$item['ketquakiemtra_id']); //Xóa kết quả kiểm tra
								
								$this->pdo->exec("DELETE FROM tbcauhoitudong WHERE kiemtra_id=$id"); //Xóa câu hỏi tự động chọn
								$this->pdo->exec("DELETE FROM tbkiemtra_cauhoi WHERE kiemtra_id=$id"); // Xóa câu hỏi kiểm tra
								$this->pdo->exec("DELETE FROM tbnhomtaikhoankiemtra WHERE kiemtra_id=$id"); //Xóa nhóm tài khoản bắt buộc kiểm tra
								$this->pdo->exec("DELETE FROM tbtaikhoankiemtra WHERE kiemtra_id=$id"); //Xóa tài khoản bắt buộc kiểm tra
								$this->pdo->exec("DELETE FROM tbkiemtra WHERE kiemtra_id=$id"); //Xóa bài kiểm tra
							}
							break;
				case 'tbtaikhoan':
							if($id==''){
								$danhsach=$this->selectall("SELECT taikhoan_id FROM tbtaikhoan",false);
								foreach($danhsach as $item)
									$this->delete('tbtaikhoan',$item['taikhoan_id']);
							}else{
								// Xóa kết quả kiểm tra của tài khoản đó //
								$danhsach=$this->selectall("SELECT ketquakiemtra_id FROM tbketquakiemtra WHERE taikhoan_id=$id",false);
								foreach ($danhsach as $item)
									$this->delete('tbketquakiemtra',$item['ketquakiemtra_id']);
								
								// Xóa câu hỏi của tài khoản đó đăng lên //
								$danhsach=$this->selectall("SELECT cauhoi_id FROM tbcauhoi WHERE taikhoan_id=$id",false);
								foreach ($danhsach as $item)
									$this->delete('tbcauhoi',$item['cauhoi_id']);
									
								// Xóa chủ đề của tài khoản đó đăng lên //
								//code here
								
								// Xóa bài kiểm tra của tài khoản đó đăng lên //
								$danhsach=$this->selectall("SELECT kiemtra_id FROM tbkiemtra WHERE taikhoan_id=$id",false);
								foreach ($danhsach as $item){
									$this->delete('tbkiemtra',$item['kiemtra_id']);
								}
								
								// Xóa tài khoản áp dụng kiểm tra //
								$this->pdo->exec("DELETE FROM tbtaikhoankiemtra WHERE taikhoan_id=$id");
								
								// Xóa tài khoản đó //
								$this->pdo->exec("DELETE FROM tbtaikhoan WHERE taikhoan_id=$id");
							}
							if($id==$_SESSION['login']['taikhoan_id']) //Nếu là tài khoản đang đăng nhập thì Logout ra
								$this->redirect('index.php?out=1');
							break;
			case 'tbnhomtaikhoan':
							if($id==''){
								$danhsach=$this->selectall("SELECT nhomtaikhoan_id FROM tbnhomtaikhoan",false);
								foreach($danhsach as $item)
									$this->delete('tbnhomtaikhoan',$item['nhomtaikhoan_id']);
							}else{
								// Xóa tài khoản thuộc nhóm đó //
								$danhsach=$this->selectall("SELECT taikhoan_id FROM tbtaikhoan WHERE nhomtaikhoan_id=$id",false);
								foreach ($danhsach as $item)
									$this->delete('tbtaikhoan',$item['taikhoan_id']);
								
								$this->pdo->exec("DELETE FROM tbnhomtaikhoankiemtra WHERE nhomtaikhoan_id=$id"); //Xóa nhóm tài khoản kiểm tra
								
								$this->pdo->exec("DELETE FROM tbnhomtaikhoan_quyen WHERE nhomtaikhoan_id=$id"); //Xóa quyền của nhóm tài khoản đó
								$this->pdo->exec("DELETE FROM tbnhomtaikhoan WHERE nhomtaikhoan_id=$id"); //Xóa nhóm tài khoản
							}
							break;
			case 'tbchude':
							if($id==''){
								$danhsach=$this->selectall("SELECT chude_id FROM tbchude",false);
								foreach($danhsach as $item)
									$this->delete('tbchude',$item['chude_id']);
							}else{
								// Xóa câu hỏi thuộc chủ đề đó //
								$danhsach=$this->selectall("SELECT cauhoi_id FROM tbcauhoi WHERE chude_id=$id",false);
								foreach($danhsach as $item)
									$this->delete('tbcauhoi',$item['cauhoi_id']);
									
								// Xóa bài kiểm tra thuộc chủ đề đó //
								$danhsach=$this->selectall("SELECT kiemtra_id FROM tbkiemtra WHERE chude_id=$id",false);
								foreach($danhsach as $item)
									$this->delete('tbkiemtra',$item['kiemtra_id']);
								
								// Xóa câu hỏi tự động chọn //
								$this->pdo->exec("DELETE FROM tbcauhoitudong WHERE chude_id=$id");
								
								// Xóa chủ đề //
								$this->pdo->exec("DELETE FROM tbchude WHERE chude_id=$id");
							}
			case 'tbmonhoc':
							if($id==''){
								$danhsach=$this->selectall("SELECT monhoc_id FROM tbmonhoc",false);
								foreach($danhsach as $item)
									$this->delete('tbmonhoc',$item['monhoc_id']);
							}else{
								// Xóa câu hỏi thuộc môn học đó //
								$danhsach=$this->selectall("SELECT cauhoi_id FROM tbcauhoi WHERE monhoc_id=$id",false);
								foreach($danhsach as $item)
									$this->delete('tbcauhoi',$item['cauhoi_id']);
									
								// Xóa bài kiểm tra thuộc môn học đó //
								$danhsach=$this->selectall("SELECT kiemtra_id FROM tbkiemtra WHERE monhoc_id=$id",false);
								foreach($danhsach as $item)
									$this->delete('tbkiemtra',$item['kiemtra_id']);
								
								// Xóa câu hỏi tự động chọn //
								$this->pdo->exec("DELETE FROM tbcauhoitudong WHERE monhoc_id=$id");
								
								// Xóa môn học //
								$this->pdo->exec("DELETE FROM tbmonhoc WHERE monhoc_id=$id");
							}
			case 'tbmucdo':
							if($id==''){
								$danhsach=$this->selectall("SELECT mucdo_id FROM tbmucdo",false);
								foreach($danhsach as $item)
									$this->delete('tbmucdo',$item['mucdo_id']);
							}else{
								// Xóa câu hỏi thuộc mức độ đó //
								$danhsach=$this->selectall("SELECT cauhoi_id FROM tbcauhoi WHERE mucdo_id=$id",false);
								foreach($danhsach as $item)
									$this->delete('tbcauhoi',$item['cauhoi_id']);
									
								// Xóa câu hỏi tự động chọn //
								$this->pdo->exec("DELETE FROM tbcauhoitudong WHERE mucdo_id=$id");
								
								// Xóa mức độ câu hỏi //
								$this->pdo->exec("DELETE FROM tbmucdo WHERE mucdo_id=$id");
							}
							break;
			case 'tbthangdiem':
							if($id==''){
								$danhsach=$this->selectall("SELECT thangdiem_id FROM tbthangdiem",false);
								foreach($danhsach as $item)
									$this->delete('tbthangdiem',$item['thangdiem_id']);
							}else{
								// Xóa bài kiểm tra thuộc chủ đề đó //
								$danhsach=$this->selectall("SELECT kiemtra_id FROM tbkiemtra WHERE thangdiem_id=$id",false);
								foreach($danhsach as $item)
									$this->delete('tbkiemtra',$item['kiemtra_id']);
									
								// Xóa xếp loại của thang điểm đó //
								$this->pdo->exec("DELETE FROM tbxeploai WHERE thangdiem_id=$id");
								
								// Xóa thang điểm đó //
								$this->pdo->exec("DELETE FROM tbthangdiem WHERE thangdiem_id=$id");
							}
							break;
			case 'tbkhoi':
							if($id==''){
								$danhsach=$this->selectall("SELECT khoi_id FROM tbkhoi",false);
								foreach($danhsach as $item)
									$this->delete('tbkhoi',$item['khoi_id']);
							}else{
								// Xóa tài khoản thuộc khối đó //
								$danhsach=$this->selectall("SELECT taikhoan_id FROM tbtaikhoan WHERE khoi_id=$id",false);
								foreach ($danhsach as $item)
									$this->delete('tbtaikhoan',$item['taikhoan_id']);
								
								// Xóa câu hỏi tự động thuộc khối đấy //
								$this->pdo->exec("DELETE FROM tbcauhoitudong WHERE khoi_id=$id");
								
								// Xóa nhóm tài khoản kiểm tra thuộc khối đấy //
								$this->pdo->exec("DELETE FROM tbnhomtaikhoankiemtra WHERE khoi_id=$id");
								
								// Xóa lớp thuộc khối đó //
								$danhsach=$this->selectall("SELECT lop_id FROM tblop WHERE khoi_id=$id",false);
								foreach ($danhsach as $item)
									$this->delete('tblop',$item['lop_id']);
								
								// Xóa khối đó //
								$this->pdo->exec("DELETE FROM tbkhoi WHERE khoi_id=$id");
							}
							break;
			case 'tblop':
							if($id==''){
								$danhsach=$this->selectall("SELECT lop_id FROM tblop",false);
								foreach($danhsach as $item)
									$this->delete('tblop',$item['lop_id']);
							}else{
								// Xóa tài khoản thuộc lớp đó //
								$danhsach=$this->selectall("SELECT taikhoan_id FROM tbtaikhoan WHERE lop_id=$id",false);
								foreach ($danhsach as $item)
									$this->delete('tbtaikhoan',$item['taikhoan_id']);
								
								// Xóa nhóm tài khoản kiểm tra thuộc lớp đấy //
								$this->pdo->exec("DELETE FROM tbnhomtaikhoankiemtra WHERE lop_id=$id");
								
								// Xóa lớp đó //
								$this->pdo->exec("DELETE FROM tblop WHERE lop_id=$id");
							}
							break;
			case 'tbtruong':
							if($id==''){
								$danhsach=$this->selectall("SELECT truong_id FROM tbtruong",false);
								foreach($danhsach as $item)
									$this->delete('tbtruong',$item['truong_id']);
							}else{
								// Xóa tài khoản thuộc trường đó //
								$danhsach=$this->selectall("SELECT taikhoan_id FROM tbtaikhoan WHERE truong_id=$id",false);
								foreach ($danhsach as $item)
									$this->delete('tbtaikhoan',$item['taikhoan_id']);
								
								// Xóa nhóm tài khoản kiểm tra thuộc trường đấy //
								$this->pdo->exec("DELETE FROM tbnhomtaikhoankiemtra WHERE truong_id=$id");
								
								// Xóa lớp thuộc trường đó //
								$danhsach=$this->selectall("SELECT lop_id FROM tblop WHERE truong_id=$id",false);
								foreach ($danhsach as $item)
									$this->delete('tblop',$item['lop_id']);
								
								// Xóa trường đó //
								$this->pdo->exec("DELETE FROM tbtruong WHERE truong_id=$id");
							}
							break;
			case 'tbdanhmuc':
							if($id==''){
								$danhsach=$this->selectall("SELECT danhmuc_id FROM tbdanhmuc",false);
								foreach($danhsach as $item)
									$this->delete('tbdanhmuc',$item['danhmuc_id']);
							}else{
								// Xóa vị trí hiển thị của danh mục đó //
								$this->pdo->exec("DELETE FROM tbdanhmuc_vitri WHERE danhmuc_id=$id");
								
								// Xóa danh mục đó //
								$this->pdo->exec("DELETE FROM tbdanhmuc WHERE danhmuc_id=$id");
							}
							break;
		}
	}
	
	function chuanhoa($str){
		while(strpos($str,'&nbsp')) //Nếu có 2 khoảng trắng liền nhau
			$str=str_replace('&nbsp',' ',$str); //Thay thế 2 khoảng trắng bằng 1 khoảng trắng
		while(strpos($str,'  ')) //Nếu có 2 khoảng trắng liền nhau
			$str=str_replace('  ',' ',$str); //Thay thế 2 khoảng trắng bằng 1 khoảng trắng
		$str=trim($str); //Loại bỏ khoảng trắng đầu và cuối
		return($str);
	}
	
	function loadcauhoikiemtra($danhsach,$sttkiemtra,$tongso,$detail){
		for ($i=$sttkiemtra; $i<$tongso; $i++){
			echo '
				<div class="cauhoi">
					<p><strong>Câu '.($i+1).': '.strip_tags($danhsach[$i]['cauhoi_tieude']).'</strong></p>
			';
			$loaicauhoi=$this->selectone("SELECT * FROM tbloaicauhoi WHERE loaicauhoi_trangthai=1 AND loaicauhoi_id=".$danhsach[$i]['loaicauhoi_id']);
			if($loaicauhoi['loaicauhoi_ma']=='tuluan'){
				echo '
					<textarea cols="130" rows="5" name="cauhoi['.$danhsach[$i]['cauhoi_id'].']" class="validate[required]" ></textarea>';
			}else{
				if(!isset($_SESSION['dapankiemtra'][$danhsach[$i]['cauhoi_id']])){
					$strlay="SELECT * FROM tbdapan WHERE cauhoi_id=".$danhsach[$i]['cauhoi_id'];
					if ($detail['kiemtra_daotraloi']==1)
						$strlay.=" ORDER BY RAND() ";
					$dapan=$this->selectall($strlay,false);
					$_SESSION['dapankiemtra'][$danhsach[$i]['cauhoi_id']]=$dapan;
				}else{
					$dapan=$_SESSION['dapankiemtra'][$danhsach[$i]['cauhoi_id']];	
				}
				
				echo '<ul>';
				foreach ($dapan as $da){
					echo '
						<li>
							<label><input type="checkbox" value="'.$da['dapan_id'].'" name="cauhoi['.$danhsach[$i]['cauhoi_id'].'][]" class="validate[required]" >'.$da['dapan_tieude'].'</label>
						</li>
					';
				}
				echo '</ul>';
			}
			echo '</div>';
		}
	}
}
?>
