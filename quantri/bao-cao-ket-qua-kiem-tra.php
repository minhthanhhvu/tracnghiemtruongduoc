<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');

/** PHPExcel_IOFactory */
require_once dirname(__FILE__) . '/../model/PHPExcel/IOFactory.php';

$objReader = PHPExcel_IOFactory::createReader('Excel5');
$objPHPExcel = $objReader->load("temp/bao-cao-ket-qua-kiem-tra.xls");

//Lấy ra danh sách dữ liệu cần xuất Excel
if(isset($_POST['btnexport'])){
	$mucchon=$_POST['cbitem'];
	$danhsach=array();
	foreach($mucchon as $item){
		$one=$lib->selectone("SELECT * FROM tbketquakiemtra WHERE ketquakiemtra_id=$item");
		$danhsach[]=$one;
	}
}else{
	$danhsach=$lib->selectall("SELECT * FROM tbketquakiemtra",false);	
}

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A5', 'Ngày thi: '. date('d/m/Y', strtotime($danhsach[0]['ketquakiemtra_ngaytao'])));

//Lấy ra phòng thi
$phongthi = $lib->selectone("SELECT * FROM tbphongthi WHERE phongthi_trangthai = 1 AND phongthi_id IN (SELECT phongthi_id FROM tbkiemtra_phongthi WHERE kiemtra_id=".$danhsach[0]['kiemtra_id'].")");
$phongthi_tieude = $phongthi['phongthi_tieude'];

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('E5', 'Phòng thi: '.$phongthi_tieude);

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A38', 'Tổng số bài: '.count($danhsach));
// Cap nhat du lieu theo cot
$i = 8;
$stt = 1;
foreach($danhsach as $item){
	$taikhoan=$lib->selectone("SELECT * FROM tbtaikhoan WHERE taikhoan_id=".$item['taikhoan_id']);
	$kiemtra=$lib->selectone("SELECT * FROM tbkiemtra WHERE kiemtra_id IN (SELECT kiemtra_id FROM tbketquakiemtra WHERE ketquakiemtra_id=".$item['ketquakiemtra_id'].")");
	

	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$i, $stt)
				->setCellValue('B'.$i, $taikhoan['taikhoan_hoten'])
				->setCellValue('C'.$i, $taikhoan['taikhoan_ngaysinh'])
				->setCellValue('D'.$i, $taikhoan['taikhoan_diachi'])
				->setCellValue('E'.$i, $taikhoan['taikhoan_lopchungchi'])
				->setCellValue('F'.$i, $taikhoan['taikhoan_sobaodanh'])
				->setCellValue('G'.$i, $item['ketquakiemtra_id'])
				->setCellValue('H'.$i, $item['ketquakiemtra_diem']);
	$i = $i + 1;
	$stt = $stt + 1;
}
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save(str_replace('.php', '.xls', __FILE__));
$lib->redirect('bao-cao-ket-qua-kiem-tra.xls');
exit;
