<?php
//Giả sử biến quy định để điều hướng phần quản lý sản phẩm: act
if(isset($_GET['act']))$act=$_GET['act']; else $act='';
$smarty->assign('act',$act);

if (isset($_GET['id']) && $act=='edit'){
	$detail=$lib->selectone("SELECT * FROM tbcauhoi WHERE cauhoi_id=".$_GET['id']);	
}

if (isset($_POST['lstsoluong']))
	$soluong=$_POST['lstsoluong'];
elseif(isset($detail))
	$soluong=$detail['cauhoi_soluong'];
else
	$soluong=4;

if (isset($_POST['lstloaicauhoi']))
	$loaicauhoi=$_POST['lstloaicauhoi'];
elseif (isset($detail))
	$loaicauhoi=$detail['loaicauhoi_id'];
else
	$loaicauhoi=1;

$loaicauhoi_ma=$lib->selectone("SELECT * FROM tbloaicauhoi WHERE loaicauhoi_trangthai=1 AND loaicauhoi_id=$loaicauhoi");
$loaicauhoi_ma=$loaicauhoi_ma['loaicauhoi_ma'];
switch ($loaicauhoi_ma){
	case 'dungsai':
					$soluong=2;
					$dapan[0]='Đúng';
					$dapan[1]='Sai';
					break;
	case 'tuluan':
					$soluong=1;
					break;
}	

if (!isset($dapan)) $dapan='';
if (!isset($loaicauhoi_ma)) $loaicauhoi_ma='';

$smarty->assign('soluong',$soluong);
$smarty->assign('loaicauhoi',$loaicauhoi);
$smarty->assign('dapan',$dapan);
$smarty->assign('loaicauhoi_ma',$loaicauhoi_ma);

//Sử dụng lệnh rẽ nhánh switch case để hiển thị từng phần quản lý sản phẩm
switch($act)
{
	case 'add': 
				if(in_array('cauhoi_them',$login['quyen'])){
					if(isset($_POST['btnup'])){
						$loaicauhoi=$_POST['lstloaicauhoi'];
						$soluong=$_POST['lstsoluong'];
						$daotraloi=$_POST['lstdaotraloi'];
						$mucdo=$_POST['lstmucdo'];
						$chude=$_POST['lstchude'];
						$cauhoi=$_POST['txtcauhoi'];
						$trangthai=$_POST['rdtrangthai'];
						
						$dapan=$_POST['dapan'];
						$dapandung=$_POST['dapandung'];
						$taikhoan_id=$_SESSION['login']['taikhoan_id'];
						$khoi=$_POST['lstkhoi'];
						$monhoc=$_POST['lstmonhoc'];
						
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="INSERT INTO tbcauhoi (loaicauhoi_id, cauhoi_soluong, cauhoi_daotraloi, mucdo_id, chude_id, cauhoi_tieude, cauhoi_trangthai, taikhoan_id, khoi_id, monhoc_id, cauhoi_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$loaicauhoi);
						$re->bindParam(2,$soluong);
						$re->bindParam(3,$daotraloi);
						$re->bindParam(4,$mucdo);
						$re->bindParam(5,$chude);
						$re->bindParam(6,$cauhoi);
						$re->bindParam(7,$trangthai);
						$re->bindParam(8,$taikhoan_id);
						$re->bindParam(9,$khoi);
						$re->bindParam(10,$monhoc);
						$re->execute();
						
						
						//Thêm đáp án vào CSDL
						$id=$lib->pdo->lastInsertId();
						foreach ($dapan as $key=>$value){
							if (in_array($key, $dapandung))
								$dung=1;
							else
								$dung=0;
							$query="INSERT INTO tbdapan (cauhoi_id, dapan_tieude, dapan_dung, dapan_trangthai) VALUES (?, ?, ?, 1)";
							$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
							$re->bindParam(1,$id);
							$re->bindParam(2,$value);
							$re->bindParam(3,$dung);
							$re->execute();
						}
						$lib->redirect('index.php?view=cauhoi');
					}
						
					$smarty->assign('action','index.php?view=cauhoi&act=add');
					$smarty->display('cauhoi/form.html');
				}
				break;
	case 'edit': 
				//Lấy biến id trên URL xuống
				if(isset($_GET['id']))
				{
					$id=$_GET['id'];
					if(isset($_POST['btnup'])){
						$loaicauhoi=$_POST['lstloaicauhoi'];
						$soluong=$_POST['lstsoluong'];
						$daotraloi=$_POST['lstdaotraloi'];
						$mucdo=$_POST['lstmucdo'];
						$cauhoi=$_POST['txtcauhoi'];
						$trangthai=$_POST['rdtrangthai'];
						
						$dapan=$_POST['dapan'];
						$dapandung=$_POST['dapandung'];
						$taikhoan_id=$_SESSION['login']['taikhoan_id'];
						$khoi=$_POST['lstkhoi'];
						$monhoc=$_POST['lstmonhoc'];
						$chude=$_POST['lstchude'];
						
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="UPDATE tbcauhoi SET loaicauhoi_id=?, cauhoi_soluong=?, cauhoi_daotraloi=?, mucdo_id=?, chude_id=?, cauhoi_tieude=?, cauhoi_trangthai=?, taikhoan_id=?, khoi_id=?, monhoc_id=? WHERE cauhoi_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$loaicauhoi);
						$re->bindParam(2,$soluong);
						$re->bindParam(3,$daotraloi);
						$re->bindParam(4,$mucdo);
						$re->bindParam(5,$chude);
						$re->bindParam(6,$cauhoi);
						$re->bindParam(7,$trangthai);
						$re->bindParam(8,$taikhoan_id);
						$re->bindParam(9,$khoi);
						$re->bindParam(10,$monhoc);
						$re->bindParam(11,$id);
						$re->execute();
						
						//Xóa đáp án cũ
						$query="DELETE FROM tbdapan WHERE cauhoi_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$id);
						$re->execute();
						
						// Cập nhật đáp án vào CSDL
						foreach ($dapan as $key=>$value){
							if (in_array($key, $dapandung))
								$dung=1;
							else
								$dung=0;
							$query="INSERT INTO tbdapan (cauhoi_id, dapan_tieude, dapan_dung, dapan_trangthai) VALUES (?, ?, ?, 1)";
							$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
							$re->bindParam(1,$id);
							$re->bindParam(2,$value);
							$re->bindParam(3,$dung);
							$re->execute();
						}
						$lib->redirect('index.php?view=cauhoi');
					}
					$detail=$lib->selectone("SELECT * FROM tbcauhoi WHERE cauhoi_id=$id");
					$smarty->assign('detail',$detail);//Khai báo biến tầng View
					
					$smarty->assign('action','index.php?view=cauhoi&act=edit&id='.$id);
					$smarty->display('cauhoi/form.html');
				}
				break;
	case 'del': //Xử lý xóa
				if(in_array('cauhoi_xoa',$login['quyen'])){
					if(isset($_GET['id'])){
						$id=$_GET['id'];
						$lib->delete('tbcauhoi',$id);
					}elseif(isset($_POST['btndel'])){
						$list=$_POST['cbitem'];
						foreach($list as $id)
							$lib->delete('tbcauhoi',$id);
					}elseif(isset($_POST['btndelall'])){
						$lib->delete('tbcauhoi');
					}
				}
				$lib->redirect('index.php?view=cauhoi');
				break;
	case 'import': //Xử lý nhập câu hỏi từ file Excel
				if(in_array('cauhoi_nhap',$login['quyen'])){
					if(isset($_POST['btnup'])){
						$path=$lib->uploadfile('nfile');
						if(!$path) $path='';
						if(!empty($path)){
							$ketqua='';
							require dirname(__FILE__) . '/../model/PHPExcel.php';
							require_once dirname(__FILE__) . '/../model/PHPExcel/IOFactory.php';
							$objPHPExcel = PHPExcel_IOFactory::load($path);
							foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
								$worksheetTitle     = $worksheet->getTitle();
								$highestRow         = $worksheet->getHighestRow(); // e.g. 10
								$highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
								$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
								$nrColumns = ord($highestColumn) - 64;
								//Hiển thị danh sách dữ liệu lấy được
								$ketqua.= "<h2>".$worksheetTitle."</h2>";
								//$ketqua.= $nrColumns . ' dòng (A-' . $highestColumn . ') ';
								//$ketqua.= ' và ' . $highestRow . ' cột.';
								$ketqua.= '<table width="100%" border="1" cellspacing="5" cellpadding="5" class="csslist">';
								for ($row = 1; $row <= $highestRow; ++ $row) {
									$ketqua.= '<tr>';
									for ($col = 0; $col < $highestColumnIndex; ++ $col) {
										$cell = $worksheet->getCellByColumnAndRow($col, $row);
										$val = $cell->getValue();
										$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
										$ketqua.= '<td>'.trim($val).'</td>';
									}
									$ketqua.= '</tr>';
								}
								$ketqua.= '</table>';
								
								//Đọc dữ liệu từ file Excel và lưu vào CSDL
								for ($row = 2; $row <= $highestRow; ++ $row) {
									$val=array();
									for ($col = 0; $col < $highestColumnIndex; ++ $col) {
									   $cell = $worksheet->getCellByColumnAndRow($col, $row);
									   $val[] = $cell->getValue();
									}
									
									//Lấy ra ID mức độ câu hỏi
									if(!empty(trim($val[1]))){ 
										$one=$lib->selectone("SELECT * FROM tbmucdo WHERE mucdo_tieude LIKE '%".trim($val[1])."%'");
										if($one>0){ 
											$mucdo_id=$one['mucdo_id'];
										}else{ 
											$query="INSERT INTO tbmucdo (mucdo_tieude, mucdo_trangthai) VALUES (?, 1)";
											$re = $lib->pdo->prepare($query);
											$re->bindParam(1,trim($val[1]));
											$re->execute();
											$mucdo_id=$lib->pdo->lastInsertId();
										}
									}else{ 
										$mucdo_id=NULL;
									}
									
									//Lấy ra ID chủ đề
									if(!empty(trim($val[2]))){
										$one=$lib->selectone("SELECT * FROM tbchude WHERE chude_tieude LIKE '%".trim($val[2])."%'");
										if($one>0){
											$chude_id=$one['chude_id'];
										}else{
											$query="INSERT INTO tbchude (chude_tieude, chude_trangthai) VALUES (?, 1)";
											$re = $lib->pdo->prepare($query);
											$re->bindParam(1,trim($val[2]));
											$re->execute();
											$chude_id=$lib->pdo->lastInsertId();
										}
									}else{
										$chude_id=NULL;
									}
									
									//Kiểm tra xem câu hỏi đã tồn tại chưa
									$one=$lib->selectone("SELECT * FROM tbcauhoi WHERE cauhoi_tieude='".trim($val[3])."'");
									if($one>0){ //Nếu chủ đề đã tồn tại
										break;
									}else{
										$cauhoi_tieude=trim($val[3]);
									}
									
									$dapan=array();
									if ($worksheetTitle=='Trắc nghiệm'){ //Nếu Import sheet chứa câu hỏi trắc nghiệm
										$dapandung=trim($val[4])-1;
										for($da=5; $da<$highestRow; $da++){
											if (!empty($val[$da]))
												$dapan[]=trim($val[$da]);
										}
										$loaicauhoi_id=$lib->selectone("SELECT loaicauhoi_id FROM tbloaicauhoi WHERE loaicauhoi_ma='tracnghiem'");
										$loaicauhoi_id=$loaicauhoi_id['loaicauhoi_id'];
									}elseif($worksheetTitle=='Tự luận'){
										$dapan[]=trim($val[4]);
										$dapandung=0;
										$loaicauhoi_id=$lib->selectone("SELECT loaicauhoi_id FROM tbloaicauhoi WHERE loaicauhoi_ma='tuluan'");
										$loaicauhoi_id=$loaicauhoi_id['loaicauhoi_id'];
									}else{
										break;	
									}
									$cauhoi_soluong=count($dapan);
									$taikhoan_id=$_SESSION['login']['taikhoan_id'];
									
									$query="INSERT INTO tbcauhoi (loaicauhoi_id, cauhoi_soluong, cauhoi_daotraloi, mucdo_id, chude_id, cauhoi_tieude, cauhoi_trangthai, taikhoan_id) VALUES (?, ?, 2, ?, ?, ?, 1, ?)";
									$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
									$re->bindParam(1,$loaicauhoi_id);
									$re->bindParam(2,$cauhoi_soluong);
									$re->bindParam(3,$mucdo_id);
									$re->bindParam(4,$chude_id);
									$re->bindParam(5,$cauhoi_tieude);
									$re->bindParam(6,$taikhoan_id);
									$re->execute();
									//print_r($re->errorInfo()); die();
									
									//Thêm đáp án vào CSDL
									$id=$lib->pdo->lastInsertId();
									foreach ($dapan as $key=>$value){
										if ($key==$dapandung)
											$dung=1;
										else
											$dung=0;
										$query="INSERT INTO tbdapan (cauhoi_id, dapan_tieude, dapan_dung, dapan_trangthai) VALUES (?, ?, ?, 1)";
										$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
										$re->bindParam(1,$id);
										$re->bindParam(2,$value);
										$re->bindParam(3,$dung);
										$re->execute();
									}
								}
							}
							$smarty->assign('ketqua',$ketqua);
						}else{
							$lib->thongbao('Bạn chưa chọn file Excel chứa ngân hàng câu hỏi.');	
						}
					}
					$smarty->display('cauhoi/import.html');
				}else{
					$lib->redirect('index.php?view=cauhoi');
				}
				break;
	default: //Lập trình hiển thị danh sách
			$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
			$strlay="SELECT * FROM tbcauhoi ";
			
			$link='index.php?view=cauhoi';//Đường link phân trang
			if(in_array('cauhoi_tim',$login['quyen'])){
				if(isset($_GET['show']) && !empty($_GET['show'])){
					$show=$_GET['show'];
					$link.='&show='.$show;
					$smarty->assign('show',$show);
				}
				
				if(isset($_GET['txtkey'])){
					$txtkey=$_GET['txtkey'];
					$strlay.=" WHERE cauhoi_tieude like '%$txtkey%'";
					$link.='&txtkey='.$txtkey;
					$smarty->assign('txtkey',$txtkey);
				}
				
				if(isset($_GET['lstchude']) && !empty($_GET['lstchude'])){
					$lstchude=$_GET['lstchude'];
					$strlay.=" AND chude_id=$lstchude ";
					$link.='&lstchude='.$lstchude;
					$smarty->assign('lstchude',$lstchude);
				}
				
				if(isset($_GET['lstmucdo']) && !empty($_GET['lstmucdo'])){
					$lstmucdo=$_GET['lstmucdo'];
					$strlay.=" AND mucdo_id=$lstmucdo ";
					$link.='&lstmucdo='.$lstmucdo;
					$smarty->assign('lstmucdo',$lstmucdo);
				}
				
				if(isset($_GET['lstkhoi']) && !empty($_GET['lstkhoi'])){
					$lstkhoi=$_GET['lstkhoi'];
					$strlay.=" AND khoi_id=$lstkhoi ";
					$link.='&lstkhoi='.$lstkhoi;
					$smarty->assign('lstkhoi',$lstkhoi);
				}
				
				if(isset($_GET['lstmonhoc']) && !empty($_GET['lstmonhoc'])){
					$lstmonhoc=$_GET['lstmonhoc'];
					$strlay.=" AND monhoc_id=$lstmonhoc ";
					$link.='&lstmonhoc='.$lstmonhoc;
					$smarty->assign('lstmonhoc',$lstmonhoc);
				}
			}
			$strlay.=" ORDER BY cauhoi_id DESC";
			$danhsach=$lib->selectall($strlay);
			
			$smarty->assign('danhsach',$danhsach);//Khai báo biến tầng view
			$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
			$smarty->display('cauhoi/danhsach.html');
}

?>