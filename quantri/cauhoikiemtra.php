<?php
//Giả sử biến quy định để điều hướng phần quản lý sản phẩm: act
if(isset($_GET['act']))$act=$_GET['act']; else $act='';
$smarty->assign('act',$act);

//Sử dụng lệnh rẽ nhánh switch case để hiển thị từng phần quản lý sản phẩm
switch($act)
{
	case 'add': 
				if(in_array('kiemtra_them',$login['quyen'])&& isset($_GET['id'])){
					$id=$_GET['id'];
					if(isset($_GET['idcauhoi'])){
						$idcauhoi=$_GET['idcauhoi'];
						$query="INSERT INTO tbkiemtra_cauhoi (cauhoi_id, kiemtra_id) VALUES (?, ?)";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$idcauhoi);
						$re->bindParam(2,$id);
						$re->execute();
					}elseif(isset($_POST['btnadd'])){
						$list=$_POST['nganhangcauhoi'];
						foreach($list as $idcauhoi){
							$query="INSERT INTO tbkiemtra_cauhoi (cauhoi_id, kiemtra_id) VALUES (?, ?)";
							$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
							$re->bindParam(1,$idcauhoi);
							$re->bindParam(2,$id);
							$re->execute();
						}
					}
					$lib->redirect('index.php?view=cauhoikiemtra&id='.$id);
				}
				break;
	case 'del': //Xử lý xóa
				if(isset($_GET['id']) && in_array('kiemtra_xoa',$login['quyen'])){
					$id=$_GET['id'];
					if(isset($_GET['idcauhoi'])){
						$idcauhoi=$_GET['idcauhoi'];	
						$lib->pdo->exec("DELETE FROM tbkiemtra_cauhoi WHERE kiemtra_id=$id AND cauhoi_id=$idcauhoi");
					}elseif(isset($_POST['btndel'])){
						$list=$_POST['cbitem'];
						foreach($list as $idcauhoi)
						$lib->pdo->exec("DELETE FROM tbkiemtra_cauhoi WHERE kiemtra_id=$id AND cauhoi_id=$idcauhoi");
					}elseif(isset($_POST['btndelall'])){
						$lib->pdo->exec("DELETE FROM tbkiemtra_cauhoi WHERE kiemtra_id=$id");
					}
					$lib->redirect('index.php?view=cauhoikiemtra&id='.$id);
				}
				break;
	case 'print': //Xử lý in bài kiểm tra
				if (isset($_GET['id'])){
					//Lấy ra danh sách câu hỏi kiểm tra
					$id=$_GET['id'];
					$smarty->assign('id',$id);
					
					//Lấy ra thông tin bài kiểm tra
					$detail=$lib->selectone("SELECT * FROM tbkiemtra WHERE kiemtra_id=$id",false);
					$smarty->assign('detail',$detail);
					
					//Lấy ra danh sách câu hỏi
					if($detail['kiemtra_tudongchon']==1){ //Nếu chọn câu hỏi tự động
						$cauhoitudong=$lib->selectall("SELECT * FROM tbcauhoitudong WHERE kiemtra_id=".$detail['kiemtra_id'],false);
						$strlay='';
						foreach($cauhoitudong as $key=>$item){
							if($key>0)
								$strlay.=' UNION ';
							$strlay.=" (SELECT * FROM tbcauhoi WHERE cauhoi_trangthai=1 ";
							if(!empty($item['mucdo_id']))
								$strlay.=" AND mucdo_id=".$item['mucdo_id'];
							if(!empty($item['chude_id']))
								$strlay.=" AND chude_id=".$item['chude_id'];
							if(!empty($item['loaicauhoi_id']))
								$strlay.=" AND loaicauhoi_id=".$item['loaicauhoi_id'];
							if(!empty($item['doitac_id']))
								$strlay.=" AND doitac_id=".$item['doitac_id'];
							if(!empty($item['khuvuc_id']))
								$strlay.=" AND khuvuc_id=".$item['khuvuc_id'];
							$strlay.=" LIMIT ".$item['cauhoitudong_soluong'].") ";
						}
					}else{ //Nếu chọn câu hỏi thủ công
						$strlay="SELECT * FROM tbcauhoi WHERE cauhoi_trangthai=1 AND cauhoi_id IN (SELECT cauhoi_id FROM tbkiemtra_cauhoi WHERE kiemtra_id=$id) ";
					}
					if($detail['kiemtra_daotraloi']==1)
						$strlay.=" ORDER BY RAND() ";
					$danhsach=$lib->selectall($strlay,false);
					$smarty->assign('danhsach',$danhsach);
					
					$smarty->display('cauhoikiemtra/print.html');
				}
				break;
	default: //Lập trình hiển thị danh sách
				if (isset($_GET['id'])){					
					//Lấy ra danh sách câu hỏi kiểm tra
					$id=$_GET['id'];
					$smarty->assign('id',$id);
					
					//Lấy ra thông tin bài kiểm tra
					$kiemtra=$lib->selectone("SELECT * FROM tbkiemtra WHERE kiemtra_id=$id",false);
					$smarty->assign('kiemtra',$kiemtra);
					
					//Lấy ra câu hỏi kiểm tra
					$strlay="SELECT * FROM tbcauhoi WHERE cauhoi_trangthai=1 AND cauhoi_id IN (SELECT cauhoi_id FROM tbkiemtra_cauhoi WHERE kiemtra_id=$id) ORDER BY cauhoi_id DESC";
					$cauhoikiemtra=$lib->selectall($strlay,false);
					$smarty->assign('cauhoikiemtra',$cauhoikiemtra);//Khai báo biến tầng view
					
					//Lấy ra ngân hàng câu hỏi
					$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
					$strlay="SELECT * FROM tbcauhoi WHERE cauhoi_id NOT IN (SELECT cauhoi_id FROM tbkiemtra_cauhoi WHERE kiemtra_id=$id) ";
					$link='index.php?view=cauhoikiemtra&id='.$id;//Đường link phân trang
					if(in_array('cauhoi_tim',$login['quyen'])){
						if(isset($_GET['show']) && !empty($_GET['show'])){
							$show=$_GET['show'];
							$link.='&show='.$show;
							$smarty->assign('show',$show);
						}
						
						if(isset($_GET['txtkey'])){
							$txtkey=$_GET['txtkey'];
							$strlay.=" AND cauhoi_tieude like '%$txtkey%'";
							$link.='&txtkey='.$txtkey;
							$smarty->assign('txtkey',$txtkey);
						}
						
						if(isset($_GET['lstchude']) && !empty($_GET['lstchude'])){
							$lstchude=$_GET['lstchude'];
							$strlay.=" AND chude_id=$lstchude ";
							$link.='&lstchude='.$lstchude;
							$smarty->assign('lstchude',$lstchude);
						}
						
						if(isset($_GET['lstmucdo']) && !empty($_GET['lstmucdo'])){
							$lstmucdo=$_GET['lstmucdo'];
							$strlay.=" AND mucdo_id=$lstmucdo ";
							$link.='&lstmucdo='.$lstmucdo;
							$smarty->assign('lstmucdo',$lstmucdo);
						}
						
						if(isset($_GET['lstkhoi']) && !empty($_GET['lstkhoi'])){
							$lstkhoi=$_GET['lstkhoi'];
							$strlay.=" AND khoi_id=$lstkhoi ";
							$link.='&lstkhoi='.$lstkhoi;
							$smarty->assign('lstkhoi',$lstkhoi);
						}
						
						if(isset($_GET['lstmonhoc']) && !empty($_GET['lstmonhoc'])){
							$lstmonhoc=$_GET['lstmonhoc'];
							$strlay.=" AND monhoc_id=$lstmonhoc ";
							$link.='&lstmonhoc='.$lstmonhoc;
							$smarty->assign('lstmonhoc',$lstmonhoc);
						}
					}
					$strlay.=" ORDER BY cauhoi_id DESC";
					$cauhoi=$lib->selectall($strlay);
					
					$smarty->assign('cauhoi',$cauhoi);//Khai báo biến tầng view
					$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
					$smarty->display('cauhoikiemtra/danhsach.html');
					
					/*
					//Lấy ra danh sách câu hỏi kiểm tra
					$id=$_GET['id'];
					$smarty->assign('id',$id);
					
					$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
					$strlay="SELECT * FROM tbcauhoi WHERE cauhoi_trangthai=1 AND cauhoi_id IN (SELECT cauhoi_id FROM tbkiemtra_cauhoi WHERE kiemtra_id=$id)";
					$link='index.php?view=cauhoikiemtra&id='.$id;//Đường link phân trang
					if(isset($_GET['txtkey'])){
						$txtkey=$_GET['txtkey'];
						$strlay.=" AND cauhoi_tieude like '%$txtkey%'";
						$link.='&txtkey='.$txtkey;
						$smarty->assign('txtkey',$txtkey);
					}
					$strlay.=" ORDER BY cauhoi_id DESC";
					$cauhoikiemtra=$lib->selectall($strlay);
					
					$smarty->assign('cauhoikiemtra',$cauhoikiemtra);//Khai báo biến tầng view
					$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
					
					//Lấy ra ngân hàng câu hỏi
					$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
					$strlay="SELECT * FROM tbcauhoi ";
					$link='index.php?view=cauhoikiemtra&id='.$id;//Đường link phân trang
					if(isset($_GET['txtkey']) && in_array('cauhoi_tim',$login['quyen'])){
						$txtkey=$_GET['txtkey'];
						$strlay.=" WHERE cauhoi_tieude like '%$txtkey%'";
						$link.='&txtkey='.$txtkey;
						$smarty->assign('txtkey',$txtkey);
					}
					$strlay.=" ORDER BY cauhoi_id DESC";
					$cauhoi=$lib->selectall($strlay);
					
					$smarty->assign('cauhoi',$cauhoi);//Khai báo biến tầng view
					$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
					$smarty->display('cauhoikiemtra/danhsach.html');
					*/
				}	
}

?>