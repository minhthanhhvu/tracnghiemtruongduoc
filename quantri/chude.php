<?php
//Giả sử biến quy định để điều hướng phần quản lý sản phẩm: act
if(isset($_GET['act']))$act=$_GET['act']; else $act='';
$smarty->assign('act',$act);

//Sử dụng lệnh rẽ nhánh switch case để hiển thị từng phần quản lý sản phẩm
switch($act)
{
	case 'add': 
				if(in_array('chude_them',$login['quyen'])){
					if(isset($_POST['btnup'])){
						$tieude=$_POST['txttieude'];
						$mota=$_POST['txtmota'];
						$trangthai=$_POST['rdtrangthai'];
						$monhoc=$_POST['lstmonhoc'];
						
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="INSERT INTO tbchude (chude_tieude, chude_trangthai, chude_mota, monhoc_id) VALUES (?, ?, ?, ?)";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$tieude);
						$re->bindParam(2,$trangthai);
						$re->bindParam(3,$mota);
						$re->bindParam(4,$monhoc);
						$re->execute();
						$lib->redirect('index.php?view=chude');
					}
					$smarty->assign('action','index.php?view=chude&act=add');
					$smarty->display('chude/form.html');
				}
				break;
	case 'edit': 
				//Lấy biến id trên URL xuống
				if(isset($_GET['id']))
				{
					$id=$_GET['id'];
					if(isset($_POST['btnup'])){
						$tieude=$_POST['txttieude'];
						$mota=$_POST['txtmota'];
						$trangthai=$_POST['rdtrangthai'];
						$monhoc=$_POST['lstmonhoc'];
						
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="UPDATE tbchude SET chude_tieude=?, chude_trangthai=?, chude_mota=?, monhoc_id=? WHERE chude_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$tieude);
						$re->bindParam(2,$trangthai);
						$re->bindParam(3,$mota);
						$re->bindParam(4,$monhoc);
						$re->bindParam(5,$id);
						$re->execute();
						$lib->redirect('index.php?view=chude');
					}
				
					$detail=$lib->selectone("SELECT * FROM tbchude WHERE chude_id=$id");
					$smarty->assign('detail',$detail);//Khai báo biến tầng View
					
					$smarty->assign('action','index.php?view=chude&act=edit&id='.$id);
					$smarty->display('chude/form.html');
				}
				break;
	case 'del': //Xử lý xóa
				if(isset($_GET['id'])){
					$id=$_GET['id'];	
					$lib->delete('tbchude',$id);
				}elseif(isset($_POST['btndel'])){
					$list=$_POST['cbitem'];
					foreach($list as $id)
						$lib->delete('tbchude',$id);
				}elseif(isset($_POST['btndelall'])){
					$lib->delete('tbchude');
				}
				$lib->redirect('index.php?view=chude');
				break;
	default: //Lập trình hiển thị danh sách
			$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
			$strlay="SELECT * FROM tbchude ";
			$link='index.php?view=chude';//Đường link phân trang
			if(isset($_GET['txtkey'])){
				$txtkey=$_GET['txtkey'];
				$strlay.=" WHERE chude_tieude like '%$txtkey%'";
				$link.='&txtkey='.$txtkey;
				$smarty->assign('txtkey',$txtkey);
			}
			$strlay.=" ORDER BY chude_id DESC";
			$danhsach=$lib->selectall($strlay);
			
			$smarty->assign('danhsach',$danhsach);//Khai báo biến tầng view
			$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
			$smarty->display('chude/danhsach.html');
}

?>