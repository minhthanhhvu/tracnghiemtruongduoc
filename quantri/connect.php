<?php
//Tầng Control của quản trị
ob_start();
session_start();
date_default_timezone_set('Asia/Ho_Chi_Minh');

//Nhung thư viện
require('../libs/Smarty.class.php');
require('../model/thuvien.php');
$smarty= new Smarty;//khởi tạo đối tượng smarty
//Khai báo lại đường dẫn 4 thư mục mặc định của smarty
$smarty->template_dir='../templates/admin/';//Thư mục chứa tầng View
$smarty->compile_dir='../templates_c/';//Thư mục biên dịch
$smarty->config_dir='../configs/';//Thư mục chứa file cấu hình
$smarty->cache_dir='../cache/';//Thư mục chứa file tạm cho web tĩnh

$smarty->configLoad('cauhinh.dat');//Đọc file cấu hình
$ch=$smarty->getConfigVars();//Lấy ra tất cả biến cấu hình
$lib=new LIB($ch);//Khởi tạo đối tượng lib
$smarty->assign('lib',$lib);

//Lấy ra danh sách cài đặt
$caidat=array();
$danhsach=$lib->selectall("SELECT * FROM tbcaidat WHERE caidat_trangthai=1 AND caidat_parent<>0",false);
foreach ($danhsach as $item){
	$caidat[$item['caidat_ma']]=$item['caidat_noidung'];
}
$_SESSION['caidat']=$caidat;
?>