<?php
//Giả sử biến quy định để điều hướng phần quản lý sản phẩm: act
if(isset($_GET['act']))$act=$_GET['act']; else $act='';
$smarty->assign('act',$act);

//Sử dụng lệnh rẽ nhánh switch case để hiển thị từng phần quản lý sản phẩm
switch($act)
{
	case 'add': 
				if(isset($_POST['btnup'])){
					$tieude=$_POST['txttieude'];
					$nhomdanhmuc=$_POST['lstnhomdanhmuc'];
					$parent=$_POST['lstparent'];
					$vitri=$_POST['vitri'];
					$trangthai=$_POST['rdtrangthai'];
					$thutu=$_POST['txtthutu'];
					
					//Xử lý cập nhật dữ liệu trên vào CSDL luôn
					$query="INSERT INTO tbdanhmuc (danhmuc_tieude, nhomdanhmuc_id, danhmuc_parent, danhmuc_trangthai, danhmuc_thutu) VALUES (?, ?, ?, ?, ?)";
					$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
					$re->bindParam(1,$tieude);
					$re->bindParam(2,$nhomdanhmuc);
					$re->bindParam(3,$parent);
					$re->bindParam(4,$trangthai);
					$re->bindParam(5,$thutu);
					$re->execute();
					
					//Thêm vị trí vào CSDL
					$id=$lib->pdo->lastInsertId();
					foreach ($vitri as $item){
						$query="INSERT INTO tbdanhmuc_vitri (danhmuc_id, vitri_id) VALUES (?, ?)";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$id);
						$re->bindParam(2,$item);
						$re->execute();
					}
					$lib->redirect('index.php?view=danhmuc');
				}
				$smarty->assign('action','index.php?view=danhmuc&act=add');
				$smarty->display('danhmuc/form.html');
				break;
	case 'edit': 
				//Lấy biến id trên URL xuống
				if(isset($_GET['id']))
				{
					$id=$_GET['id'];
					if(isset($_POST['btnup'])){
						$tieude=$_POST['txttieude'];
						$nhomdanhmuc=$_POST['lstnhomdanhmuc'];
						$parent=$_POST['lstparent'];
						$vitri=$_POST['vitri'];
						$trangthai=$_POST['rdtrangthai'];
						$thutu=$_POST['txtthutu'];
						
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="UPDATE tbdanhmuc SET danhmuc_tieude=?, nhomdanhmuc_id=?, danhmuc_parent=?, danhmuc_trangthai=?, danhmuc_thutu=? WHERE danhmuc_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$tieude);
						$re->bindParam(2,$nhomdanhmuc);
						$re->bindParam(3,$parent);
						$re->bindParam(4,$trangthai);
						$re->bindParam(5,$thutu);
						$re->bindParam(6,$id);
						$re->execute();
						
						//Xóa vị trị hiển thị cũ
						$query="DELETE FROM tbdanhmuc_vitri WHERE danhmuc_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$id);
						$re->execute();
						
						//Thêm vị trí hiển thị mới
						foreach ($vitri as $item){
							$query="INSERT INTO tbdanhmuc_vitri (danhmuc_id, vitri_id) VALUES (?, ?)";
							$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
							$re->bindParam(1,$id);
							$re->bindParam(2,$item);
							$re->execute();
						}
						
						$lib->redirect('index.php?view=danhmuc');
					}
				
					$detail=$lib->selectone("SELECT * FROM tbdanhmuc WHERE danhmuc_id=$id");
					$smarty->assign('detail',$detail);//Khai báo biến tầng View
					
					$smarty->assign('action','index.php?view=danhmuc&act=edit&id='.$id);
					$smarty->display('danhmuc/form.html');
				}
				break;
	case 'del': //Xử lý xóa
				if(isset($_GET['id'])){
					$id=$_GET['id'];	
					$lib->delete('tbdanhmuc',$id);
				}elseif(isset($_POST['btndel'])){
					$list=$_POST['cbitem'];
					foreach($list as $id)
						$lib->delete('tbdanhmuc',$id);
				}elseif(isset($_POST['btndelall'])){
					$lib->delete('tbdanhmuc');
				}
				$lib->redirect('index.php?view=danhmuc');
				break;
	default: //Lập trình hiển thị danh sách
			$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
			$strlay="SELECT * FROM tbdanhmuc ";
			$link='index.php?view=danhmuc';//Đường link phân trang
			if(isset($_GET['txtkey'])){
				$txtkey=$_GET['txtkey'];
				$strlay.=" WHERE danhmuc_tieude like '%$txtkey%'";
				$link.='&txtkey='.$txtkey;
				$smarty->assign('txtkey',$txtkey);
			}
			$strlay.=" ORDER BY danhmuc_id DESC";
			$danhsach=$lib->selectall($strlay);
			
			$smarty->assign('danhsach',$danhsach);//Khai báo biến tầng view
			$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
			$smarty->display('danhmuc/danhsach.html');
}

?>