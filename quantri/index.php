<?php
ob_start();
require_once('connect.php');
if(isset($_SESSION['login']['quyen']) && in_array('truycapquantri',$_SESSION['login']['quyen'])){
	//Xử lý logout
	if(isset($_GET['out']) && $_GET['out']==1)
	{
		unset($_SESSION['login']);
		setcookie('nhopass','',time()-1);
		unset($_COOKIE['nhopass']);
		$lib->redirect('/dang-nhap.html');
	}else{
		$login=$_SESSION['login'];
		$smarty->assign('login',$login);
		
		if(isset($_GET['view']))$view=$_GET['view']; else $view='';
		$smarty->assign('view',$view);
		
		if(isset($_GET['act']))$act=$_GET['act']; else $act='';
		$smarty->assign('act',$act);
	
		//Gọi hiển thị tầng view
		$smarty->display('header.html');
		//Phan noi dung thay doi
		//Lấy biến view trên URL xuống: qua PT GET
		if(isset($_GET['view']))$view=$_GET['view'];else $view='trangchu';//Nếu ko tồn tại View trên URL thì view = trống
		
		if(file_exists($view.'.php'))
			require($view.'.php');
		else
			require('trangchu.php');
		$smarty->display('footer.html');
	}
}else{
	$lib->redirect('/');
}
?>