<?php
//Giả sử biến quy định để điều hướng phần quản lý sản phẩm: act
if(isset($_GET['act']))$act=$_GET['act']; else $act='';
$smarty->assign('act',$act);

//Sử dụng lệnh rẽ nhánh switch case để hiển thị từng phần quản lý sản phẩm
switch($act)
{
	case 'edit':
				if(isset($_GET['kiemtra_id']) && isset($_GET['ketquakiemtra_id'])){
					$kiemtra_id=$_GET['kiemtra_id'];
					$ketquakiemtra_id=$_GET['ketquakiemtra_id'];
					
					$detail=$lib->selectone("SELECT * FROM tbkiemtra WHERE kiemtra_id=$kiemtra_id");
					$smarty->assign('detail',$detail);
					
					$ketquakiemtra=$lib->selectone("SELECT * FROM tbketquakiemtra WHERE ketquakiemtra_id=$ketquakiemtra_id");
					$smarty->assign('ketquakiemtra',$ketquakiemtra);
					
					$danhsach=$lib->selectall("SELECT * FROM tbketquakiemtra_cauhoi WHERE ketquakiemtra_id=$ketquakiemtra_id",false);
					$smarty->assign('danhsach',$danhsach);
					
					//Tính số lần được phép thử lại
					$one=$lib->selectone("SELECT * FROM tbkiemtra WHERE kiemtra_id=".$detail['kiemtra_id']);
					if ($detail['kiemtra_batdau'] <= date('Y-m-d H:i:s') && $detail['kiemtra_ketthuc'] >= date('Y-m-d H:i:s')){ //Nếu bài kiểm tra có còn hạn
						if($one['kiemtra_laphangngay']==1){ //Nếu bài kiểm tra lặp hàng ngày
							$ketquakiemtra=$lib->rowCount("SELECT 1 FROM tbketquakiemtra WHERE kiemtra_id=".$detail['kiemtra_id']." AND taikhoan_id=".$detail['taikhoan_id']." AND DATE_FORMAT(ketquakiemtra_ngaytao,'%Y-%m-%d') = DATE_FORMAT(NOW(),'%Y-%m-%d') ");
						}else{ //Nếu bài kiểm tra k lặp hàng ngày
							$ketquakiemtra=$lib->rowCount("SELECT 1 FROM tbketquakiemtra WHERE kiemtra_id=".$detail['kiemtra_id']." AND taikhoan_id=".$detail['taikhoan_id']);
						}
						$thulai=$one['kiemtra_thulai']-$ketquakiemtra;
					}else{ //Nếu bài kiểm tra đã quá hạn
						$thulai=0;
					}
					$smarty->assign('thulai',$thulai);
					$smarty->display('ketquakiemtra/form.html');
				}
				break;
	case 'del': //Xử lý xóa
				if(isset($_GET['id'])){
					$id=$_GET['id'];	
					$lib->delete('tbketquakiemtra',$id);
				}elseif(isset($_POST['btndel'])){
					$list=$_POST['cbitem'];
					foreach($list as $id)
						$lib->delete('tbketquakiemtra',$id);
				}elseif(isset($_POST['btndelall'])){
					$lib->delete('tbketquakiemtra');
				}elseif(isset($_POST['btnexport']) || isset($_POST['btnexportall'])){
					require 'bao-cao-ket-qua-kiem-tra.php';
				}
				$lib->redirect('index.php?view=ketquakiemtra');
				break;
	default: //Lập trình hiển thị danh sách
			$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
			$strlay="SELECT * FROM tbketquakiemtra ";
			$link='index.php?view=ketquakiemtra';//Đường link phân trang
			if(in_array('cauhoi_tim',$login['quyen'])){
				$strlay.=" WHERE ketquakiemtra_trangthai>=0 ";
				if(isset($_GET['show']) && !empty($_GET['show'])){
					$show=$_GET['show'];
					$link.='&show='.$show;
					$smarty->assign('show',$show);
				}
				
				if(isset($_GET['lstidkiemtra']) && !empty($_GET['lstidkiemtra'])){
					$lstidkiemtra=$_GET['lstidkiemtra'];
					$strlay.=" AND kiemtra_id=$lstidkiemtra ";
					$link.='&lstidkiemtra='.$lstidkiemtra;
					$smarty->assign('lstidkiemtra',$lstidkiemtra);
				}
				
				if(isset($_GET['txtbatdau']) && !empty($_GET['txtbatdau'])){
					$txtbatdau=$lib->date_convert($_GET['txtbatdau'],'yyyy-mm-dd hh:ii:ss');
					$strlay.=" AND ketquakiemtra_ngaytao >= '$txtbatdau' ";
					$link.='&txtbatdau='.$txtbatdau;
					$smarty->assign('txtbatdau',$txtbatdau);
				}
				
				if(isset($_GET['txtketthuc']) && !empty($_GET['txtketthuc'])){
					$txtketthuc=$lib->date_convert($_GET['txtketthuc'],'yyyy-mm-dd hh:ii:ss');
					$strlay.=" AND ketquakiemtra_ngaytao <= '$txtketthuc' ";
					$link.='&txtketthuc='.$txtketthuc;
					$smarty->assign('txtketthuc',$txtketthuc);
				}
				
				if(isset($_GET['lstnhomtaikhoan']) && !empty($_GET['lstnhomtaikhoan'])){
					$lstnhomtaikhoan=$_GET['lstnhomtaikhoan'];
					$strlay.=" AND taikhoan_id IN (SELECT taikhoan_id FROM tbtaikhoan WHERE nhomtaikhoan_id=$lstnhomtaikhoan) ";
					$link.='&lstnhomtaikhoan='.$lstnhomtaikhoan;
					$smarty->assign('lstnhomtaikhoan',$lstnhomtaikhoan);
				}
				
				if(isset($_GET['lsttaikhoan']) && !empty($_GET['lsttaikhoan'])){
					$lsttaikhoan=$_GET['lsttaikhoan'];
					$strlay.=" AND taikhoan_id=$lsttaikhoan ";
					$link.='&lsttaikhoan='.$lsttaikhoan;
					$smarty->assign('lsttaikhoan',$lsttaikhoan);
				}
				
				if(isset($_GET['lstphongthi']) && !empty($_GET['lstphongthi'])){
					$lstphongthi=$_GET['lstphongthi'];
					$strlay.=" AND taikhoan_id IN (SELECT taikhoan_id FROM tbtaikhoan WHERE phongthi_id=$lstphongthi) ";
					$link.='&lstmucdo='.$lstphongthi;
					$smarty->assign('lstphongthi',$lstphongthi);
				}
				
				if(isset($_GET['lstkhoi']) && !empty($_GET['lstkhoi'])){
					$lstkhoi=$_GET['lstkhoi'];
					$strlay.=" AND taikhoan_id IN (SELECT taikhoan_id FROM tbtaikhoan WHERE khoi_id=$lstkhoi) ";
					$link.='&lstkhoi='.$lstkhoi;
					$smarty->assign('lstkhoi',$lstkhoi);
				}
				
				if(isset($_GET['lstlop']) && !empty($_GET['lstlop'])){
					$lstlop=$_GET['lstlop'];
					$strlay.=" AND taikhoan_id IN (SELECT taikhoan_id FROM tbtaikhoan WHERE lop_id=$lstlop) ";
					$link.='&lstlop='.$lstlop;
					$smarty->assign('lstlop',$lstlop);
				}
			}
			$strlay.=" ORDER BY ketquakiemtra_id DESC";
			$danhsach=$lib->selectall($strlay);
			
			$smarty->assign('danhsach',$danhsach);//Khai báo biến tầng view
			$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
			$smarty->display('ketquakiemtra/danhsach.html');
}

?>