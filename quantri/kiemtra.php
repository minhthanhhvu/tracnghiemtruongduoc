<?php
//Giả sử biến quy định để điều hướng phần quản lý sản phẩm: act
if(isset($_GET['act']))$act=$_GET['act']; else $act='';
$smarty->assign('act',$act);

if (isset($_POST) && !empty($_POST)){
	$post=$_POST;
}elseif (isset($_GET['id']) && $act=='edit'){
	$post=$lib->selectone("SELECT * FROM tbkiemtra WHERE kiemtra_id=".$_GET['id']);
}

if (isset($post)) $smarty->assign('post',$post);
	
//Sử dụng lệnh rẽ nhánh switch case để hiển thị từng phần quản lý sản phẩm
switch($act)
{
	case 'add': 
				if(isset($_POST['btnup'])){
					$taikhoan_id=$_POST['taikhoan_id'];
					if(isset($_POST['kiemtra_yeucau']))
						$kiemtra_yeucau=1;
					else
						$kiemtra_yeucau=0;
					if(isset($_POST['kiemtra_batbuoc']))
						$kiemtra_batbuoc=1;
					else
						$kiemtra_batbuoc=0;
					if(isset($_POST['kiemtra_laphangngay']))
						$kiemtra_laphangngay=1;
					else
						$kiemtra_laphangngay=0;
					if(isset($_POST['kiemtra_tudongchon'])){
						$kiemtra_tudongchon=1;
						$kiemtra_slnhom=$_POST['kiemtra_slnhom'];
					}else{
						$kiemtra_tudongchon=0;
						$kiemtra_slnhom=1;
					}
					$kiemtra_tieude=$_POST['kiemtra_tieude'];
					$kiemtra_mota=$_POST['kiemtra_mota'];
					$kiemtra_batdau=$lib->date_convert($_POST['kiemtra_batdau'],'yyyy-mm-dd hh:ii:ss');
					$kiemtra_ketthuc=$lib->date_convert($_POST['kiemtra_ketthuc'],'yyyy-mm-dd hh:ii:ss');
					$kiemtra_thulai=$_POST['kiemtra_thulai'];
					$kiemtra_kieuhienthi=$_POST['kiemtra_kieuhienthi'];
					if (!empty($_POST['thangdiem_id']))
						$thangdiem_id=$_POST['thangdiem_id'];
					else
						$thangdiem_id=NULL;
					$kiemtra_emailkhac=$_POST['kiemtra_emailkhac'];
					
					if (isset($_POST['kiemtra_emailthisinh']))
						$kiemtra_emailthisinh=1;
					else
						$kiemtra_emailthisinh=0;
					
					$taikhoankiemtra=$_POST['taikhoankiemtra'];
					
					if (isset($_POST['kiemtra_daocauhoi']))
						$kiemtra_daocauhoi=1;
					else
						$kiemtra_daocauhoi=0;
					
					if (isset($_POST['kiemtra_daotraloi']))
						$kiemtra_daotraloi=1;
					else
						$kiemtra_daotraloi=0;
					
					if (isset($_POST['kiemtra_hiendiem']))
						$kiemtra_hiendiem=1;
					else
						$kiemtra_hiendiem=0;
					
					if (isset($_POST['kiemtra_hienxeploai']))
						$kiemtra_hienxeploai=1;
					else
						$kiemtra_hienxeploai=0;
					
					if (isset($_POST['kiemtra_xemlaibaithi']))
						$kiemtra_xemlaibaithi=1;
					else
						$kiemtra_xemlaibaithi=0;
					
					$kiemtra_trangthai=$_POST['kiemtra_trangthai'];
					if (isset($_POST['kiemtra_khongthoigian'])){
						$kiemtra_khongthoigian=1;
						$kiemtra_thoigian=0;
					}else{
						$kiemtra_khongthoigian=0;
						$kiemtra_thoigian=$lib->date_convert($_POST['kiemtra_thoigian'],'hh:ii:ss');
					}
					if (!empty($_POST['chude_id']))
						$chude_id=$_POST['chude_id'];
					else
						$chude_id=NULL;
					if (!empty($_POST['monhoc_id']))
						$monhoc_id=$_POST['monhoc_id'];
					else
						$monhoc_id=NULL;
					if (!empty($_POST['khoi_id']))
						$khoi_id=$_POST['khoi_id'];
					else
						$khoi_id=NULL;

					
					//Xử lý cập nhật dữ liệu trên vào CSDL luôn
					$query="INSERT INTO tbkiemtra (taikhoan_id, kiemtra_laphangngay, kiemtra_slnhom, kiemtra_tieude, kiemtra_mota, kiemtra_batdau, kiemtra_ketthuc, kiemtra_thoigian, kiemtra_thulai, kiemtra_kieuhienthi, thangdiem_id, kiemtra_emailkhac, kiemtra_emailthisinh, kiemtra_daocauhoi, kiemtra_daotraloi, kiemtra_hiendiem, kiemtra_hienxeploai, kiemtra_xemlaibaithi, kiemtra_trangthai, kiemtra_khongthoigian, kiemtra_tudongchon, chude_id, kiemtra_ngaytao, kiemtra_batbuoc, kiemtra_yeucau, monhoc_id, khoi_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?, ?, ?, ?)";
					$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
					$re->bindParam(1,$taikhoan_id);
					$re->bindParam(2,$kiemtra_laphangngay);
					$re->bindParam(3,$kiemtra_slnhom);
					$re->bindParam(4,$kiemtra_tieude);
					$re->bindParam(5,$kiemtra_mota);
					$re->bindParam(6,$kiemtra_batdau);
					$re->bindParam(7,$kiemtra_ketthuc);
					$re->bindParam(8,$kiemtra_thoigian);
					$re->bindParam(9,$kiemtra_thulai);
					$re->bindParam(10,$kiemtra_kieuhienthi);
					$re->bindParam(11,$thangdiem_id);
					$re->bindParam(12,$kiemtra_emailkhac);
					$re->bindParam(13,$kiemtra_emailthisinh);
					$re->bindParam(14,$kiemtra_daocauhoi);
					$re->bindParam(15,$kiemtra_daotraloi);
					$re->bindParam(16,$kiemtra_hiendiem);
					$re->bindParam(17,$kiemtra_hienxeploai);
					$re->bindParam(18,$kiemtra_xemlaibaithi);
					$re->bindParam(19,$kiemtra_trangthai);
					$re->bindParam(20,$kiemtra_khongthoigian);
					$re->bindParam(21,$kiemtra_tudongchon);
					$re->bindParam(22,$chude_id);
					$re->bindParam(23,$kiemtra_batbuoc);
					$re->bindParam(24,$kiemtra_yeucau);
					$re->bindParam(25,$monhoc_id);
					$re->bindParam(26,$khoi_id);
					$re->execute();
					
					$id=$lib->pdo->lastInsertId();
					if ($kiemtra_tudongchon==1){
						$kiemtra_slnhom=$_POST['kiemtra_slnhom'];
						$soluong=$_POST['soluong'];
						$mucdo=$_POST['mucdo'];
						$chude=$_POST['chude'];
						$khoi=$_POST['khoi'];
						$monhoc=$_POST['monhoc'];
						$loaicauhoi=$_POST['loaicauhoi'];
						$phongthi=$_POST['phongthi'];
						
						for($i=0; $i<$kiemtra_slnhom; $i++){
							if(!empty($mucdo[$i])){
								$mucdo_id=$mucdo[$i];	
							}else{
								$mucdo_id=NULL;	
							}
							
							if(!empty($chude[$i])){
								$chude_id=$chude[$i];	
							}else{
								$chude_id=NULL;	
							}
							
							if(!empty($monhoc[$i])){
								$monhoc_id=$monhoc[$i];	
							}else{
								$monhoc_id=NULL;	
							}
							
							if(!empty($khoi[$i])){
								$khoi_id=$khoi[$i];	
							}else{
								$khoi_id=NULL;	
							}
							
							if(!empty($loaicauhoi[$i])){
								$loaicauhoi_id=$loaicauhoi[$i];	
							}else{
								$loaicauhoi_id=NULL;	
							}
							
							if(!empty($phongthi[$i])){
								$phongthi_id=$phongthi[$i];	
							}else{
								$phongthi_id=NULL;	
							}
							
							$query="INSERT INTO tbcauhoitudong (cauhoitudong_soluong, mucdo_id, chude_id, khoi_id, monhoc_id, loaicauhoi_id, kiemtra_id, phongthi_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
							$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
							$re->bindParam(1,$soluong[$i]);
							$re->bindParam(2,$mucdo_id);
							$re->bindParam(3,$chude_id);
							$re->bindParam(4,$khoi_id);
							$re->bindParam(5,$monhoc_id);
							$re->bindParam(6,$loaicauhoi_id);
							$re->bindParam(7,$id);
							$re->bindParam(8,$phongthi_id);
							$re->execute();
						}
					}

					//Thêm danh sách phòng thi kiểm tra
					if (!empty($_POST['phongthi_id'])){
						$phongthi_id=$_POST['phongthi_id'];
						foreach ($phongthi_id as $item){
							$query="INSERT INTO tbkiemtra_phongthi (phongthi_id, kiemtra_id) VALUES (?, ?)";

							$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
							$re->bindParam(1,$item);
							$re->bindParam(2,$id);
							$re->execute();
						}
					}
					
					//Thêm nhóm tài khoản kiểm tra
					if (isset($_POST['lopkiemtra'])){
						$lopkiemtra=$_POST['lopkiemtra'];	
					}else{
						$lopkiemtra='';	
					}
					if (!empty($lopkiemtra)){
						foreach ($lopkiemtra as $item){
							$arr=explode(';',$item);
							$nhomtaikhoan_id=$arr[0];
							$truong_id=$arr[1];
							$khoi_id=$arr[2];
							$lop_id=$arr[3];
							
							$query="INSERT INTO tbnhomtaikhoankiemtra (nhomtaikhoan_id, truong_id, khoi_id, lop_id, kiemtra_id) VALUES (?, ?, ?, ?, ?)";
							$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
							$re->bindParam(1,$nhomtaikhoan_id);
							$re->bindParam(2,$truong_id);
							$re->bindParam(3,$khoi_id);
							$re->bindParam(4,$lop_id);
							$re->bindParam(5,$id);
							$re->execute();
						}
					}
					
					$arr=explode(';',$taikhoankiemtra);
					if (!empty($arr)){
						foreach ($arr as $item){
							$query="INSERT INTO tbtaikhoankiemtra (taikhoan_id, kiemtra_id) VALUES (?, ?)";
							$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
							$re->bindParam(1,$item);
							$re->bindParam(2,$id);
							$re->execute();
						}
					}
					$lib->redirect('index.php?view=kiemtra');
				}
				$smarty->assign('action','index.php?view=kiemtra&act=add');
				$smarty->display('kiemtra/form.html');
				break;
	case 'edit': 
				//Lấy biến id trên URL xuống
				if(isset($_GET['id']))
				{
					$id=$_GET['id'];
					if(isset($_POST['btnup'])){
						$taikhoan_id=$_POST['taikhoan_id'];
						if(isset($_POST['kiemtra_yeucau']))
							$kiemtra_yeucau=1;
						else
							$kiemtra_yeucau=0;
						if(isset($_POST['kiemtra_batbuoc']))
							$kiemtra_batbuoc=1;
						else
							$kiemtra_batbuoc=0;
						if(isset($_POST['kiemtra_laphangngay']))
							$kiemtra_laphangngay=1;
						else
							$kiemtra_laphangngay=0;
						if(isset($_POST['kiemtra_tudongchon'])){
							$kiemtra_tudongchon=1;
							$kiemtra_slnhom=$_POST['kiemtra_slnhom'];
						}else{
							$kiemtra_tudongchon=0;
							$kiemtra_slnhom=1;
						}
						$kiemtra_tieude=$_POST['kiemtra_tieude'];
						$kiemtra_mota=$_POST['kiemtra_mota'];
						$kiemtra_batdau=$lib->date_convert($_POST['kiemtra_batdau'],'yyyy-mm-dd hh:ii:ss');
						$kiemtra_ketthuc=$lib->date_convert($_POST['kiemtra_ketthuc'],'yyyy-mm-dd hh:ii:ss');
						$kiemtra_thulai=$_POST['kiemtra_thulai'];
						$kiemtra_kieuhienthi=$_POST['kiemtra_kieuhienthi'];
						if (!empty($_POST['thangdiem_id']))
							$thangdiem_id=$_POST['thangdiem_id'];
						else
							$thangdiem_id=NULL;
						$kiemtra_emailkhac=$_POST['kiemtra_emailkhac'];
						
						if (isset($_POST['kiemtra_emailthisinh']))
							$kiemtra_emailthisinh=1;
						else
							$kiemtra_emailthisinh=0;
						
						$taikhoankiemtra=$_POST['taikhoankiemtra'];
						
						if (isset($_POST['kiemtra_daocauhoi']))
							$kiemtra_daocauhoi=1;
						else
							$kiemtra_daocauhoi=0;
						
						if (isset($_POST['kiemtra_daotraloi']))
							$kiemtra_daotraloi=1;
						else
							$kiemtra_daotraloi=0;
						
						if (isset($_POST['kiemtra_hiendiem']))
							$kiemtra_hiendiem=1;
						else
							$kiemtra_hiendiem=0;
						
						if (isset($_POST['kiemtra_hienxeploai']))
							$kiemtra_hienxeploai=1;
						else
							$kiemtra_hienxeploai=0;
						
						if (isset($_POST['kiemtra_xemlaibaithi']))
							$kiemtra_xemlaibaithi=1;
						else
							$kiemtra_xemlaibaithi=0;
						
						$kiemtra_trangthai=$_POST['kiemtra_trangthai'];
						if (isset($_POST['kiemtra_khongthoigian'])){
							$kiemtra_khongthoigian=1;
							$kiemtra_thoigian=0;
						}else{
							$kiemtra_khongthoigian=0;
							$kiemtra_thoigian=$lib->date_convert($_POST['kiemtra_thoigian'],'hh:ii:ss');
						}
						if (!empty($_POST['chude_id']))
							$chude_id=$_POST['chude_id'];
						else
							$chude_id=NULL;
						if (!empty($_POST['monhoc_id']))
							$monhoc_id=$_POST['monhoc_id'];
						else
							$monhoc_id=NULL;
						if (!empty($_POST['khoi_id']))
							$khoi_id=$_POST['khoi_id'];
						else
							$khoi_id=NULL;
						
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="UPDATE tbkiemtra SET taikhoan_id=?, kiemtra_laphangngay=?, kiemtra_slnhom=?, kiemtra_tieude=?, kiemtra_mota=?, kiemtra_batdau=?, kiemtra_ketthuc=?, kiemtra_thoigian=?, kiemtra_thulai=?, kiemtra_kieuhienthi=?, thangdiem_id=?, kiemtra_emailkhac=?, kiemtra_emailthisinh=?, kiemtra_daocauhoi=?, kiemtra_daotraloi=?, kiemtra_hiendiem=?, kiemtra_hienxeploai=?, kiemtra_xemlaibaithi=?, kiemtra_trangthai=?, kiemtra_khongthoigian=?, kiemtra_tudongchon=?, chude_id=?, kiemtra_ngaysua=NOW(), kiemtra_batbuoc=?, kiemtra_yeucau=?, monhoc_id=?, khoi_id=? WHERE kiemtra_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$taikhoan_id);
						$re->bindParam(2,$kiemtra_laphangngay);
						$re->bindParam(3,$kiemtra_slnhom);
						$re->bindParam(4,$kiemtra_tieude);
						$re->bindParam(5,$kiemtra_mota);
						$re->bindParam(6,$kiemtra_batdau);
						$re->bindParam(7,$kiemtra_ketthuc);
						$re->bindParam(8,$kiemtra_thoigian);
						$re->bindParam(9,$kiemtra_thulai);
						$re->bindParam(10,$kiemtra_kieuhienthi);
						$re->bindParam(11,$thangdiem_id);
						$re->bindParam(12,$kiemtra_emailkhac);
						$re->bindParam(13,$kiemtra_emailthisinh);
						$re->bindParam(14,$kiemtra_daocauhoi);
						$re->bindParam(15,$kiemtra_daotraloi);
						$re->bindParam(16,$kiemtra_hiendiem);
						$re->bindParam(17,$kiemtra_hienxeploai);
						$re->bindParam(18,$kiemtra_xemlaibaithi);
						$re->bindParam(19,$kiemtra_trangthai);
						$re->bindParam(20,$kiemtra_khongthoigian);
						$re->bindParam(21,$kiemtra_tudongchon);
						$re->bindParam(22,$chude_id);
						$re->bindParam(23,$kiemtra_batbuoc);
						$re->bindParam(24,$kiemtra_yeucau);
						$re->bindParam(25,$monhoc_id);
						$re->bindParam(26,$khoi_id);
						$re->bindParam(27,$id);
						$re->execute();
						
						//Xóa phòng thi cũ
							$query="DELETE FROM tbkiemtra_phongthi WHERE kiemtra_id=?";
							$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
							$re->bindParam(1,$id);
							$re->execute();

						//Thêm danh sách phòng thi kiểm tra
						if (!empty($_POST['phongthi_id'])){
							$phongthi_id=$_POST['phongthi_id'];
							foreach ($phongthi_id as $item){
								$query="INSERT INTO tbkiemtra_phongthi (phongthi_id, kiemtra_id) VALUES (?, ?)";

								$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
								$re->bindParam(1,$item);
								$re->bindParam(2,$id);
								$re->execute();
							}
						}

						//Sửa tạo câu hỏi tự động
						if ($kiemtra_tudongchon==1){
							//Xóa câu hỏi tự động cũ
							$query="DELETE FROM tbcauhoitudong WHERE kiemtra_id=?";
							$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
							$re->bindParam(1,$id);
							$re->execute();
							
							//Thêm câu hỏi tự động mới
							$kiemtra_slnhom=$_POST['kiemtra_slnhom'];
							$soluong=$_POST['soluong'];
							$mucdo=$_POST['mucdo'];
							$chude=$_POST['chude'];
							$khoi=$_POST['khoi'];
							$monhoc=$_POST['monhoc'];
							$loaicauhoi=$_POST['loaicauhoi'];
							$phongthi=$_POST['phongthi'];
							
							for($i=0; $i<$kiemtra_slnhom; $i++){
								if(!empty($mucdo[$i])){
									$mucdo_id=$mucdo[$i];	
								}else{
									$mucdo_id=NULL;	
								}
								
								if(!empty($chude[$i])){
									$chude_id=$chude[$i];	
								}else{
									$chude_id=NULL;	
								}
								
								if(!empty($monhoc[$i])){
									$monhoc_id=$monhoc[$i];	
								}else{
									$monhoc_id=NULL;	
								}
								
								if(!empty($khoi[$i])){
									$khoi_id=$khoi[$i];	
								}else{
									$khoi_id=NULL;	
								}
								
								if(!empty($loaicauhoi[$i])){
									$loaicauhoi_id=$loaicauhoi[$i];	
								}else{
									$loaicauhoi_id=NULL;	
								}
								
								if(!empty($phongthi[$i])){
									$phongthi_id=$phongthi[$i];	
								}else{
									$phongthi_id=NULL;	
								}
								
								$query="INSERT INTO tbcauhoitudong (cauhoitudong_soluong, mucdo_id, chude_id, khoi_id, monhoc_id, loaicauhoi_id, kiemtra_id, phongthi_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
								$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
								$re->bindParam(1,$soluong[$i]);
								$re->bindParam(2,$mucdo_id);
								$re->bindParam(3,$chude_id);
								$re->bindParam(4,$khoi_id);
								$re->bindParam(5,$monhoc_id);
								$re->bindParam(6,$loaicauhoi_id);
								$re->bindParam(7,$id);
								$re->bindParam(8,$phongthi_id);
								$re->execute();
								//print_r($re->errorInfo()); die();
							}
						}
						
						//Xóa nhóm tài khoản kiểm tra cũ
						$query="DELETE FROM tbnhomtaikhoankiemtra WHERE kiemtra_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$id);
						$re->execute();
						//Thêm nhóm tài khoản kiểm tra mới
						if (isset($_POST['lopkiemtra'])){
							$lopkiemtra=$_POST['lopkiemtra'];	
						}else{
							$lopkiemtra='';	
						}
						if (!empty($lopkiemtra)){
							foreach ($lopkiemtra as $item){
								$arr=explode(';',$item);
								$nhomtaikhoan_id=$arr[0];
								$truong_id=$arr[1];
								$khoi_id=$arr[2];
								$lop_id=$arr[3];
								
								$query="INSERT INTO tbnhomtaikhoankiemtra (nhomtaikhoan_id, truong_id, khoi_id, lop_id, kiemtra_id) VALUES (?, ?, ?, ?, ?)";
								$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
								$re->bindParam(1,$nhomtaikhoan_id);
								$re->bindParam(2,$truong_id);
								$re->bindParam(3,$khoi_id);
								$re->bindParam(4,$lop_id);
								$re->bindParam(5,$id);
								$re->execute();
							}
						}
						
						//Sửa danh sách tài khoản kiểm tra
						$query="DELETE FROM tbtaikhoankiemtra WHERE kiemtra_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$id);
						$re->execute();
						$arr=explode(';',$taikhoankiemtra);
						if (!empty($arr)){
							foreach ($arr as $item){
								$query="INSERT INTO tbtaikhoankiemtra (taikhoan_id, kiemtra_id) VALUES (?, ?)";
								$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
								$re->bindParam(1,$item);
								$re->bindParam(2,$id);
								$re->execute();
							}
						}
						$lib->redirect('index.php?view=kiemtra');
					}
					$smarty->assign('action','index.php?view=kiemtra&act=edit&id='.$id);
					$smarty->display('kiemtra/form.html');
				}
				break;
	case 'del': //Xử lý xóa
				if(isset($_GET['id'])){
					$id=$_GET['id'];
					$lib->delete('tbkiemtra',$id);
				}elseif(isset($_POST['btndel'])){
					$list=$_POST['cbitem'];
					foreach($list as $id){
						$lib->delete('tbkiemtra',$id);
					}
				}elseif(isset($_POST['btndelall'])){
					$lib->delete('tbkiemtra');
				}
				$lib->redirect('index.php?view=kiemtra');
				break;
	default: //Lập trình hiển thị danh sách
				$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
				$strlay="SELECT * FROM tbkiemtra ";
				$link='index.php?view=kiemtra';//Đường link phân trang
				if(in_array('kiemtra_tim',$login['quyen'])){
					if(isset($_GET['show']) && !empty($_GET['show'])){
						$show=$_GET['show'];
						$link.='&show='.$show;
						$smarty->assign('show',$show);
					}
					
					if(isset($_GET['txtkey'])){
						$txtkey=$_GET['txtkey'];
						$strlay.=" WHERE kiemtra_tieude like '%$txtkey%'";
						$link.='&txtkey='.$txtkey;
						$smarty->assign('txtkey',$txtkey);
					}
					
					if(isset($_GET['txtbatdau']) && !empty($_GET['txtbatdau'])){
						$txtbatdau=$lib->date_convert($_GET['txtbatdau'],'yyyy-mm-dd hh:ii:ss');
						$strlay.=" AND kiemtra_ngaytao >= '$txtbatdau' ";
						$link.='&txtbatdau='.$txtbatdau;
						$smarty->assign('txtbatdau',$txtbatdau);
					}
					
					if(isset($_GET['txtketthuc']) && !empty($_GET['txtketthuc'])){
						$txtketthuc=$lib->date_convert($_GET['txtketthuc'],'yyyy-mm-dd hh:ii:ss');
						$strlay.=" AND kiemtra_ngaytao <= '$txtketthuc' ";
						$link.='&txtketthuc='.$txtketthuc;
						$smarty->assign('txtketthuc',$txtketthuc);
					}
					
					if(isset($_GET['lstchude']) && !empty($_GET['lstchude'])){
						$lstchude=$_GET['lstchude'];
						$strlay.=" AND chude_id=$lstchude ";
						$link.='&lstchude='.$lstchude;
						$smarty->assign('lstchude',$lstchude);
					}
					
					if(isset($_GET['lstkhoi']) && !empty($_GET['lstkhoi'])){
						$lstkhoi=$_GET['lstkhoi'];
						$strlay.=" AND khoi_id=$lstkhoi ";
						$link.='&lstkhoi='.$lstkhoi;
						$smarty->assign('lstkhoi',$lstkhoi);
					}
					
					if(isset($_GET['lstmonhoc']) && !empty($_GET['lstmonhoc'])){
						$lstmonhoc=$_GET['lstmonhoc'];
						$strlay.=" AND monhoc_id=$lstmonhoc ";
						$link.='&lstmonhoc='.$lstmonhoc;
						$smarty->assign('lstmonhoc',$lstmonhoc);
					}
					
					if(isset($_GET['lstphongthi']) && !empty($_GET['lstphongthi'])){
						$lstphongthi=$_GET['lstphongthi'];
						$strlay.=" AND phongthi_id=$lstphongthi ";
						$link.='&lstphongthi='.$lstphongthi;
						$smarty->assign('lstphongthi',$lstphongthi);
					}
				}
				$strlay.=" ORDER BY kiemtra_id DESC";
				$danhsach=$lib->selectall($strlay);
				
				$smarty->assign('danhsach',$danhsach);//Khai báo biến tầng view
				$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
				$smarty->display('kiemtra/danhsach.html');
}

?>