<?php
//Giả sử biến quy định để điều hướng phần quản lý sản phẩm: act
if(isset($_GET['act']))$act=$_GET['act']; else $act='';
$smarty->assign('act',$act);

//Sử dụng lệnh rẽ nhánh switch case để hiển thị từng phần quản lý sản phẩm
switch($act)
{
	case 'add': 
				if(isset($_POST['btnup'])){
					$tieude=$_POST['txttieude'];
					$trangthai=$_POST['rdtrangthai'];
					$khoi=$_POST['lstkhoi'];
					$truong=$_POST['lsttruong'];
					
					//Xử lý cập nhật dữ liệu trên vào CSDL luôn
					$query="INSERT INTO tblop (lop_tieude, lop_trangthai, khoi_id, truong_id) VALUES (?, ?, ?, ?)";
					$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
					$re->bindParam(1,$tieude);
					$re->bindParam(2,$trangthai);
					$re->bindParam(3,$khoi);
					$re->bindParam(4,$truong);
					$re->execute();
					
					$lib->redirect('index.php?view=lop');
				}
				$smarty->assign('action','index.php?view=lop&act=add');
				$smarty->display('lop/form.html');
				break;
	case 'edit': 
				//Lấy biến id trên URL xuống
				if(isset($_GET['id']))
				{
					$id=$_GET['id'];
					if(isset($_POST['btnup'])){
						$tieude=$_POST['txttieude'];
						$trangthai=$_POST['rdtrangthai'];
						$khoi=$_POST['lstkhoi'];
						$truong=$_POST['lsttruong'];
												
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="UPDATE tblop SET lop_tieude=?, lop_trangthai=?, khoi_id=?, truong_id=? WHERE lop_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$tieude);
						$re->bindParam(2,$trangthai);
						$re->bindParam(3,$khoi);
						$re->bindParam(4,$truong);
						$re->bindParam(5,$id);
						$re->execute();
						$lib->redirect('index.php?view=lop');
					}
				
					$detail=$lib->selectone("SELECT * FROM tblop WHERE lop_id=$id");
					$smarty->assign('detail',$detail);//Khai báo biến tầng View
					
					$smarty->assign('action','index.php?view=lop&act=edit&id='.$id);
					$smarty->display('lop/form.html');
				}
				break;
	case 'del': //Xử lý xóa
				if(isset($_GET['id'])){
					$id=$_GET['id'];	
					$lib->delete('tblop',$id);
				}elseif(isset($_POST['btndel'])){
					$list=$_POST['cbitem'];
					foreach($list as $id)
						$lib->delete('tblop',$id);
				}elseif(isset($_POST['btndelall'])){
					$lib->delete('tblop');
				}
				$lib->redirect('index.php?view=lop');
				break;
	default: //Lập trình hiển thị danh sách
			$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
			$strlay="SELECT * FROM tblop ";
			$link='index.php?view=lop';//Đường link phân trang
			if(isset($_GET['txtkey'])){
				$txtkey=$_GET['txtkey'];
				$strlay.=" WHERE lop_tieude like '%$txtkey%'";
				$link.='&txtkey='.$txtkey;
				$smarty->assign('txtkey',$txtkey);
			}
			$strlay.=" ORDER BY lop_id DESC";
			$danhsach=$lib->selectall($strlay);
			
			$smarty->assign('danhsach',$danhsach);//Khai báo biến tầng view
			$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
			$smarty->display('lop/danhsach.html');
}

?>