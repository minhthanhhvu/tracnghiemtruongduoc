<?php
//Giả sử biến quy định để điều hướng phần quản lý sản phẩm: act
if(isset($_GET['act']))$act=$_GET['act']; else $act='';
$smarty->assign('act',$act);

//Sử dụng lệnh rẽ nhánh switch case để hiển thị từng phần quản lý sản phẩm
switch($act)
{
	case 'add': 
				if(in_array('monhoc_them',$login['quyen'])){
					if(isset($_POST['btnup'])){
						$tieude=$_POST['txttieude'];
						$mota=$_POST['txtmota'];
						$trangthai=$_POST['rdtrangthai'];
						$tohop=$_POST['lsttohop'];
						
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="INSERT INTO tbmonhoc (monhoc_tieude, monhoc_trangthai, monhoc_mota, tohop_id) VALUES (?, ?, ?, ?)";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$tieude);
						$re->bindParam(2,$trangthai);
						$re->bindParam(3,$mota);
						$re->bindParam(4,$tohop);
						$re->execute();
						$lib->redirect('index.php?view=monhoc');
					}
					$smarty->assign('action','index.php?view=monhoc&act=add');
					$smarty->display('monhoc/form.html');
				}
				break;
	case 'edit': 
				//Lấy biến id trên URL xuống
				if(isset($_GET['id']))
				{
					$id=$_GET['id'];
					if(isset($_POST['btnup'])){
						$tieude=$_POST['txttieude'];
						$mota=$_POST['txtmota'];
						$trangthai=$_POST['rdtrangthai'];
						$tohop=$_POST['lsttohop'];
						
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="UPDATE tbmonhoc SET monhoc_tieude=?, monhoc_trangthai=?, monhoc_mota=?, tohop_id=? WHERE monhoc_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$tieude);
						$re->bindParam(2,$trangthai);
						$re->bindParam(3,$mota);
						$re->bindParam(4,$tohop);
						$re->bindParam(5,$id);
						$re->execute();
						$lib->redirect('index.php?view=monhoc');
					}
				
					$detail=$lib->selectone("SELECT * FROM tbmonhoc WHERE monhoc_id=$id");
					$smarty->assign('detail',$detail);//Khai báo biến tầng View
					
					$smarty->assign('action','index.php?view=monhoc&act=edit&id='.$id);
					$smarty->display('monhoc/form.html');
				}
				break;
	case 'del': //Xử lý xóa
				if(isset($_GET['id'])){
					$id=$_GET['id'];	
					$lib->delete('tbmonhoc',$id);
				}elseif(isset($_POST['btndel'])){
					$list=$_POST['cbitem'];
					foreach($list as $id)
						$lib->delete('tbmonhoc',$id);
				}elseif(isset($_POST['btndelall'])){
					$lib->delete('tbmonhoc');
				}
				$lib->redirect('index.php?view=monhoc');
				break;
	default: //Lập trình hiển thị danh sách
			$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
			$strlay="SELECT * FROM tbmonhoc ";
			$link='index.php?view=monhoc';//Đường link phân trang
			if(isset($_GET['txtkey'])){
				$txtkey=$_GET['txtkey'];
				$strlay.=" WHERE monhoc_tieude like '%$txtkey%'";
				$link.='&txtkey='.$txtkey;
				$smarty->assign('txtkey',$txtkey);
			}
			$strlay.=" ORDER BY monhoc_id DESC";
			$danhsach=$lib->selectall($strlay);
			
			$smarty->assign('danhsach',$danhsach);//Khai báo biến tầng view
			$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
			$smarty->display('monhoc/danhsach.html');
}

?>