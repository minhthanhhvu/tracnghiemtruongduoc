<?php
//Giả sử biến quy định để điều hướng phần quản lý sản phẩm: act
if(isset($_GET['act']))$act=$_GET['act']; else $act='';
$smarty->assign('act',$act);

//Sử dụng lệnh rẽ nhánh switch case để hiển thị từng phần quản lý sản phẩm
switch($act)
{
	case 'add': 
				if(isset($_POST['btnup'])){
					$tieude=$_POST['txttieude'];
					$diem=$_POST['txtdiem'];
					$trangthai=$_POST['rdtrangthai'];
					
					//Xử lý cập nhật dữ liệu trên vào CSDL luôn
					$query="INSERT INTO tbmucdo (mucdo_tieude, mucdo_trangthai, mucdo_diem) VALUES (?, ?, ?)";
					$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
					$re->bindParam(1,$tieude);
					$re->bindParam(2,$trangthai);
					$re->bindParam(3,$diem);
					$re->execute();
					$lib->redirect('index.php?view=mucdo');
				}
				$smarty->assign('action','index.php?view=mucdo&act=add');
				$smarty->display('mucdo/form.html');
				break;
	case 'edit': 
				//Lấy biến id trên URL xuống
				if(isset($_GET['id']))
				{
					$id=$_GET['id'];
					if(isset($_POST['btnup'])){
						$tieude=$_POST['txttieude'];
						$diem=$_POST['txtdiem'];
						$trangthai=$_POST['rdtrangthai'];
						
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="UPDATE tbmucdo SET mucdo_tieude=?, mucdo_trangthai=?, mucdo_diem=? WHERE mucdo_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$tieude);
						$re->bindParam(2,$trangthai);
						$re->bindParam(3,$diem);
						$re->bindParam(4,$id);
						$re->execute();
						$lib->redirect('index.php?view=mucdo');
					}
				
					$detail=$lib->selectone("SELECT * FROM tbmucdo WHERE mucdo_id=$id");
					$smarty->assign('detail',$detail);//Khai báo biến tầng View
					
					$smarty->assign('action','index.php?view=mucdo&act=edit&id='.$id);
					$smarty->display('mucdo/form.html');
				}
				break;
	case 'del': //Xử lý xóa
				if(isset($_GET['id'])){
					$id=$_GET['id'];	
					$lib->delete('tbmucdo',$id);
				}elseif(isset($_POST['btndel'])){
					$list=$_POST['cbitem'];
					foreach($list as $id)
						$lib->delete('tbmucdo',$id);
				}elseif(isset($_POST['btndelall'])){
					$lib->delete('tbmucdo');
				}
				$lib->redirect('index.php?view=mucdo');
				break;
	default: //Lập trình hiển thị danh sách
			$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
			$strlay="SELECT * FROM tbmucdo ";
			$link='index.php?view=mucdo';//Đường link phân trang
			if(isset($_GET['txtkey'])){
				$txtkey=$_GET['txtkey'];
				$strlay.=" WHERE mucdo_tieude like '%$txtkey%'";
				$link.='&txtkey='.$txtkey;
				$smarty->assign('txtkey',$txtkey);
			}
			$strlay.=" ORDER BY mucdo_id DESC";
			$danhsach=$lib->selectall($strlay);
			
			$smarty->assign('danhsach',$danhsach);//Khai báo biến tầng view
			$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
			$smarty->display('mucdo/danhsach.html');
}

?>