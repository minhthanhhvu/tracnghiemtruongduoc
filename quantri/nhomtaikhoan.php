<?php
//Giả sử biến quy định để điều hướng phần quản lý sản phẩm: act
if(isset($_GET['act']))$act=$_GET['act']; else $act='';
$smarty->assign('act',$act);

//Sử dụng lệnh rẽ nhánh switch case để hiển thị từng phần quản lý sản phẩm
switch($act)
{
	case 'add': 
				if(isset($_POST['btnup'])){
					$tieude=$_POST['txttieude'];
					$trangthai=$_POST['rdtrangthai'];
					$macdinh=$_POST['lstmacdinh'];
					$quyen=$_POST['quyen'];
					
					//Xử lý cập nhật dữ liệu trên vào CSDL luôn
					$query="INSERT INTO tbnhomtaikhoan (nhomtaikhoan_tieude, nhomtaikhoan_trangthai, nhomtaikhoan_macdinh) VALUES (?, ?, ?)";
					$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
					$re->bindParam(1,$tieude);
					$re->bindParam(2,$trangthai);
					$re->bindParam(3,$macdinh);
					$re->execute();
					
					//Thêm danh sách các quyền vào CSDL
					$id=$lib->pdo->lastInsertId();
					foreach ($quyen as $item){
						$query="INSERT INTO tbnhomtaikhoan_quyen (nhomtaikhoan_id, quyen_id) VALUES (?, ?)";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$id);
						$re->bindParam(2,$item);
						$re->execute();
					}
					$lib->redirect('index.php?view=nhomtaikhoan');
				}
				$smarty->assign('action','index.php?view=nhomtaikhoan&act=add');
				$smarty->display('nhomtaikhoan/form.html');
				break;
	case 'edit': 
				//Lấy biến id trên URL xuống
				if(isset($_GET['id']))
				{
					$id=$_GET['id'];
					if(isset($_POST['btnup'])){
						$tieude=$_POST['txttieude'];
						$trangthai=$_POST['rdtrangthai'];
						$macdinh=$_POST['lstmacdinh'];
						if (isset($_POST['quyen']))
							$quyen=$_POST['quyen'];
						else
							$quyen=array();
						
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="UPDATE tbnhomtaikhoan SET nhomtaikhoan_tieude=?, nhomtaikhoan_trangthai=?, nhomtaikhoan_macdinh=? WHERE nhomtaikhoan_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$tieude);
						$re->bindParam(2,$trangthai);
						$re->bindParam(3,$macdinh);
						$re->bindParam(4,$id);
						$re->execute();
						
						//Sửa danh sách các quyền
						$query="DELETE FROM tbnhomtaikhoan_quyen WHERE nhomtaikhoan_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$id);
						$re->execute();
						
						foreach ($quyen as $item){
							$query="INSERT INTO tbnhomtaikhoan_quyen (nhomtaikhoan_id, quyen_id) VALUES (?, ?)";
							$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
							$re->bindParam(1,$id);
							$re->bindParam(2,$item);
							$re->execute();
						}
						
						//Cập nhật lại danh sách quyền vào SESSION
						if($id==$_SESSION['login']['nhomtaikhoan_id']){
							//Load quyền của tài khoản lưu vào SESSION
							$danhsach=$lib->selectall("SELECT quyen_ma FROM tbquyen WHERE quyen_trangthai=1 AND quyen_id IN (SELECT quyen_id FROM tbnhomtaikhoan_quyen WHERE nhomtaikhoan_id=".$_SESSION['login']['nhomtaikhoan_id'].") ",false);
							$quyen=array();
							foreach ($danhsach as $item){
								$quyen[]=$item['quyen_ma'];
							}
							$_SESSION['login']['quyen']=$quyen;
						}
						
						$lib->redirect('index.php?view=nhomtaikhoan');
					}
				
					$detail=$lib->selectone("SELECT * FROM tbnhomtaikhoan WHERE nhomtaikhoan_id=$id");
					$smarty->assign('detail',$detail);//Khai báo biến tầng View
					
					$smarty->assign('action','index.php?view=nhomtaikhoan&act=edit&id='.$id);
					$smarty->display('nhomtaikhoan/form.html');
				}
				break;
	case 'del': //Xử lý xóa
				if(isset($_GET['id'])){
					$id=$_GET['id'];
					$lib->delete('tbnhomtaikhoan',$id);
				}elseif(isset($_POST['btndel'])){
					$list=$_POST['cbitem'];
					foreach($list as $id)
						$lib->delete('tbnhomtaikhoan',$id);
				}elseif(isset($_POST['btndelall'])){
					$lib->delete('tbnhomtaikhoan');
				}
				$lib->redirect('index.php?view=nhomtaikhoan');
				break;
	default: //Lập trình hiển thị danh sách
			$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
			$strlay="SELECT * FROM tbnhomtaikhoan ";
			$link='index.php?view=nhomtaikhoan';//Đường link phân trang
			if(isset($_GET['txtkey'])){
				$txtkey=$_GET['txtkey'];
				$strlay.=" WHERE nhomtaikhoan_tieude like '%$txtkey%'";
				$link.='&txtkey='.$txtkey;
				$smarty->assign('txtkey',$txtkey);
			}
			$strlay.=" ORDER BY nhomtaikhoan_id DESC";
			$danhsach=$lib->selectall($strlay);
			
			$smarty->assign('danhsach',$danhsach);//Khai báo biến tầng view
			$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
			$smarty->display('nhomtaikhoan/danhsach.html');
}

?>