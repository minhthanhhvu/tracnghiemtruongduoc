<?php
//Giả sử biến quy định để điều hướng phần quản lý sản phẩm: act
if(isset($_GET['act']))$act=$_GET['act']; else $act='';
$smarty->assign('act',$act);

//Sử dụng lệnh rẽ nhánh switch case để hiển thị từng phần quản lý sản phẩm
switch($act)
{
	case 'add': 
				//Xử lý dữ liệu lấy từ form sản phẩm
				if(isset($_POST['btnup'])){
					$user=$_POST['txtuser'];
					$email=$_POST['txtemail'];
					$pass=md5($_POST['txtpass']);
					$hoten=$_POST['txthoten'];
					$gioitinh=$_POST['lstgioitinh'];
					$ngaysinh=$lib->date_convert($_POST['txtngaysinh']);
					$diachi=$_POST['txtdiachi'];
					$phone=$_POST['txtphone'];
					$quyen=$_POST['lstnhom'];
					$trangthai=$_POST['rdtrangthai'];
					$nhom=$_POST['lstnhom'];
					if(isset($_POST['lsttruong']))
						$truong=$_POST['lsttruong'];
					else
						$truong=NULL;
					if(isset($_POST['lstkhoi']))
						$khoi=$_POST['lstkhoi'];
					else
						$khoi=NULL;
					if(isset($_POST['lstlop']))
						$lop=$_POST['lstlop'];
					else
						$lop=NULL;
					if(isset($_POST['lstphongthi']))
						$phongthi=$_POST['lstphongthi'];
					else
						$phongthi=NULL;
					$sobaodanh = $_POST['txtsobaodanh'];
					$lopchungchi = $_POST['txtlopchungchi'];
					
					//Xử lý cập nhật dữ liệu trên vào CSDL luôn
					$query="INSERT INTO tbtaikhoan (taikhoan_user, taikhoan_email, taikhoan_pass, taikhoan_hoten, taikhoan_gioitinh, taikhoan_ngaysinh, taikhoan_diachi, taikhoan_phone, taikhoan_ngaytao, taikhoan_trangthai, nhomtaikhoan_id, phongthi_id, taikhoan_sobaodanh, taikhoan_lopchungchi) VALUES (?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?, ?, ?, ?, ?)";
					$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
					$re->bindParam(1,$user);
					$re->bindParam(2,$email);
					$re->bindParam(3,$pass);
					$re->bindParam(4,$hoten);
					$re->bindParam(5,$gioitinh);
					$re->bindParam(6,$ngaysinh);
					$re->bindParam(7,$diachi);
					$re->bindParam(8,$phone);
					$re->bindParam(9,$trangthai);
					$re->bindParam(10,$nhom);
					$re->bindParam(11,$phongthi);
					$re->bindParam(12,$sobaodanh);
					$re->bindParam(13,$lopchungchi);
					$re->execute();
					$lib->redirect('index.php?view=taikhoan');
				}
				$smarty->assign('action','index.php?view=taikhoan&act=add');
				$smarty->display('taikhoan/form.html');
				break;
	case 'edit': 
				//Lấy biến id trên URL xuống
				if(isset($_GET['id']))
				{
					$id=$_GET['id'];
					if(isset($_POST['btnup'])){
						if($_POST['txtoldpass']==$_POST['txtpass'])
							$pass=$_POST['txtpass'];
						else
							$pass=md5($_POST['txtpass']);
						$user=$_POST['txtuser'];
						$email=$_POST['txtemail'];
						$hoten=$_POST['txthoten'];
						$gioitinh=$_POST['lstgioitinh'];
						$ngaysinh=$lib->date_convert($_POST['txtngaysinh']);
						$diachi=$_POST['txtdiachi'];
						$phone=$_POST['txtphone'];
						$quyen=$_POST['lstnhom'];
						$trangthai=$_POST['rdtrangthai'];
						$nhom=$_POST['lstnhom'];
						if(isset($_POST['lsttruong']))
							$truong=$_POST['lsttruong'];
						else
							$truong=NULL;
						if(isset($_POST['lstkhoi']))
							$khoi=$_POST['lstkhoi'];
						else
							$khoi=NULL;
						if(isset($_POST['lstlop']))
							$lop=$_POST['lstlop'];
						else
							$lop=NULL;
						if(isset($_POST['lstphongthi']))
							$phongthi=$_POST['lstphongthi'];
						else
							$phongthi=NULL;
						$sobaodanh = $_POST['txtsobaodanh'];
						$lopchungchi = $_POST['txtlopchungchi'];
						
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="UPDATE tbtaikhoan SET taikhoan_user=?, taikhoan_email=?, taikhoan_pass=?, taikhoan_hoten=?, taikhoan_gioitinh=?, taikhoan_ngaysinh=?, taikhoan_diachi=?, taikhoan_phone=?, taikhoan_ngaysua=NOW(), taikhoan_trangthai=?, nhomtaikhoan_id=?, phongthi_id=?, taikhoan_sobaodanh=?, taikhoan_lopchungchi=? WHERE taikhoan_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$user);
						$re->bindParam(2,$email);
						$re->bindParam(3,$pass);
						$re->bindParam(4,$hoten);
						$re->bindParam(5,$gioitinh);
						$re->bindParam(6,$ngaysinh);
						$re->bindParam(7,$diachi);
						$re->bindParam(8,$phone);
						$re->bindParam(9,$trangthai);
						$re->bindParam(10,$nhom);
						$re->bindParam(11,$phongthi);
						$re->bindParam(12,$sobaodanh);
						$re->bindParam(13,$lopchungchi);
						$re->bindParam(14,$id);
						$re->execute();
						
						if($id==$_SESSION['login']['taikhoan_id']){
							$strlog="SELECT * FROM tbtaikhoan WHERE taikhoan_id=$id AND taikhoan_trangthai=1 ";
							$log=$lib->selectone($strlog);
							$_SESSION['login']=$log;
						}
						$lib->redirect('index.php?view=taikhoan');
					}
				
					$detail=$lib->selectone("SELECT * FROM tbtaikhoan WHERE taikhoan_id=$id");
					$smarty->assign('detail',$detail);//Khai báo biến tầng View
					
					$smarty->assign('action','index.php?view=taikhoan&act=edit&id='.$id);
					$smarty->display('taikhoan/form.html');
				}
				break;
	case 'del': //Xử lý xóa tài khoản
				if(isset($_GET['id'])){
					$id=$_GET['id'];
					$lib->delete('tbtaikhoan',$id);
				}elseif(isset($_POST['btndel'])){
					$list=$_POST['cbitem'];
					foreach($list as $id){
						$lib->delete('tbtaikhoan',$id);
					}
				}elseif(isset($_POST['btndelall'])){
					$lib->delete('tbtaikhoan');
				}
				$lib->redirect('index.php?view=taikhoan');
				break;
	case 'import': //Xử lý nhập câu hỏi từ file Excel
				if(in_array('cauhoi_nhap',$login['quyen'])){
					if(isset($_POST['btnup'])){
						$path=$lib->uploadfile('nfile');
						if(!$path) $path='';
						if(!empty($path)){
							$ketqua='';
							require dirname(__FILE__) . '/../model/PHPExcel.php';
							require_once dirname(__FILE__) . '/../model/PHPExcel/IOFactory.php';
							$objPHPExcel = PHPExcel_IOFactory::load($path);
							foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
								$worksheetTitle     = $worksheet->getTitle();
								$highestRow         = $worksheet->getHighestRow(); // e.g. 10
								$highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
								$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
								$nrColumns = ord($highestColumn) - 64;
								//Hiển thị danh sách dữ liệu lấy được
								$ketqua.= "<h2>Danh sách thành viên trong file Excel</h2>";
								$ketqua.= '<table width="100%" border="1" cellspacing="5" cellpadding="5" class="csslist">';
								for ($row = 1; $row <= $highestRow; ++ $row) {
									$ketqua.= '<tr>';
									for ($col = 0; $col < $highestColumnIndex; ++ $col) {
										$cell = $worksheet->getCellByColumnAndRow($col, $row);
										$val = $cell->getValue();
										if(PHPExcel_Shared_Date::isDateTime($cell)) {
											 $val = date('d/m/Y', PHPExcel_Shared_Date::ExcelToPHP($val)); 
										}
										$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
										$ketqua.= '<td>'.$val.'</td>';
									}
									$ketqua.= '</tr>';
								}
								$ketqua.= '</table>';
								
								//Đọc dữ liệu từ file Excel và lưu vào CSDL
								for ($row = 2; $row <= $highestRow; ++ $row) {
									$val=array();
									for ($col = 0; $col < $highestColumnIndex; ++ $col) {
									   $cell = $worksheet->getCellByColumnAndRow($col, $row);
									   $val[] = $cell->getValue();
									}	
									//Lấy ra ID phòng thi
									if(!empty(trim($val[1]))){
										$one=$lib->selectone("SELECT * FROM tbphongthi WHERE phongthi_tieude LIKE '%".trim($val[1])."%'");
										if($one>0){ 
											$phongthi_id=$one['phongthi_id'];
										}else{ 
											$query="INSERT INTO tbphongthi (phongthi_tieude, phongthi_trangthai) VALUES (?, 1)";
											$re = $lib->pdo->prepare($query);
											$re->bindParam(1,trim($val[1]));
											$re->execute();
											$phongthi_id=$lib->pdo->lastInsertId();
										}
									}else{ 
										$phongthi_id=NULL;
									}

									$taikhoan_sobaodanh = trim($val[2]);
																			
									//Kiểm tra xem tài khoản đã tồn tại chưa
									$one=$lib->selectone("SELECT * FROM tbtaikhoan WHERE taikhoan_user LIKE '%".trim($val[3])."%'");
									if($one>0){ //Nếu tài khoản đã tồn tại thì bỏ qua
										break;
									}else{
										$taikhoan_user=trim($val[3]);
									}
									$taikhoan_pass=md5(trim($val[4]));
									
									//Lấy ra ID nhóm thành viên
									$one=$lib->selectone("SELECT * FROM tbnhomtaikhoan WHERE nhomtaikhoan_tieude LIKE '%".trim($val[5])."%'");
									if($one>0)
										$nhomtaikhoan_id=$one['nhomtaikhoan_id'];
									else
										$nhomtaikhoan_id=NULL;
									
									if(trim($val[6])=='Nam'){
										$taikhoan_gioitinh=1;	
									}elseif(trim($val[6])=='Nữ'){
										$taikhoan_gioitinh=2;
									}else{
										$taikhoan_gioitinh=0;
									}
									
									$taikhoan_hoten=trim(trim($val[7]));
									$taikhoan_ngaysinh = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP(trim($val[8])));
									$taikhoan_lopchungchi=trim($val[9]);
									$taikhoan_diachi=trim($val[10]);
									$taikhoan_phone=trim($val[11]);
									$taikhoan_email=null;
									
									$query="INSERT INTO tbtaikhoan (taikhoan_user, taikhoan_email, taikhoan_pass, taikhoan_hoten, taikhoan_gioitinh, taikhoan_ngaysinh, taikhoan_diachi, taikhoan_phone, taikhoan_ngaytao, taikhoan_trangthai, nhomtaikhoan_id, phongthi_id, taikhoan_lopchungchi, taikhoan_sobaodanh) VALUES (?, ?, ?, ?, ?, ?, ?, ?, NOW(), 1, ?, ?, ?, ?)";
									$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
									$re->bindParam(1,$taikhoan_user);
									$re->bindParam(2,$taikhoan_email);
									$re->bindParam(3,$taikhoan_pass);
									$re->bindParam(4,$taikhoan_hoten);
									$re->bindParam(5,$taikhoan_gioitinh);
									$re->bindParam(6,$taikhoan_ngaysinh);
									$re->bindParam(7,$taikhoan_diachi);
									$re->bindParam(8,$taikhoan_phone);
									$re->bindParam(9,$nhomtaikhoan_id);
									$re->bindParam(10,$phongthi_id);
									$re->bindParam(11,$taikhoan_lopchungchi);
									$re->bindParam(12,$taikhoan_sobaodanh);
									$re->execute();
									//print_r($re->errorInfo()); die();
								}
							}
							$smarty->assign('ketqua',$ketqua);
						}else{
							$lib->thongbao('Bạn chưa chọn file Excel chứa ngân hàng câu hỏi.');	
						}
					}
					$smarty->display('taikhoan/import.html');
				}else{
					$lib->redirect('index.php?view=cauhoi');
				}
				break;
	default: //Lập trình hiển thị danh sách tài khoản
			$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
			$strlay="SELECT * FROM tbtaikhoan ";
			$link='index.php?view=taikhoan';//Đường link phân trang
			if(in_array('cauhoi_tim',$login['quyen'])){
                $strlay.= " WHERE taikhoan_id > 0 ";

				if(isset($_GET['show']) && !empty($_GET['show'])){
					$show=$_GET['show'];
					$link.='&show='.$show;
					$smarty->assign('show',$show);
				}
				
				if(isset($_GET['txtuser']) && !empty($_GET['txtuser'])){
					$txtuser=$_GET['txtuser'];
					$strlay.=" WHERE taikhoan_user LIKE '%$txtuser%'";
					$link.='&txtuser='.$txtuser;
					$smarty->assign('txtuser',$txtuser);
				}
				
				if(isset($_GET['txthoten']) && !empty($_GET['txthoten'])){
					$txthoten=$_GET['txthoten'];
					$strlay.=" AND taikhoan_hoten LIKE '%$txthoten%'";
					$link.='&txthoten='.$txthoten;
					$smarty->assign('txthoten',$txthoten);
				}
				
				if(isset($_GET['txtemail']) && !empty($_GET['txtemail'])){
					$txtemail=$_GET['txtemail'];
					$strlay.=" AND taikhoan_email LIKE '%$txtemail%'";
					$link.='&txtemail='.$txtemail;
					$smarty->assign('txtemail',$txtemail);
				}
				
				if(isset($_GET['txtphone']) && !empty($_GET['txtemail'])){
					$txtphone=$_GET['txtphone'];
					$strlay.=" AND taikhoan_phone LIKE '%$txtphone%'";
					$link.='&txtphone='.$txtphone;
					$smarty->assign('txtphone',$txtphone);
				}

				if(isset($_GET['lstnhomtaikhoan']) && !empty($_GET['lstnhomtaikhoan'])){
					$lstnhomtaikhoan=$_GET['lstnhomtaikhoan'];
					$strlay.=" AND taikhoan_id IN (SELECT taikhoan_id FROM tbtaikhoan WHERE nhomtaikhoan_id=$lstnhomtaikhoan) ";
					$link.='&lstnhomtaikhoan='.$lstnhomtaikhoan;
					$smarty->assign('lstnhomtaikhoan',$lstnhomtaikhoan);
				}
				
				if(isset($_GET['lstphongthi']) && !empty($_GET['lstphongthi'])){
					$lstphongthi=$_GET['lstphongthi'];
					$strlay.=" AND taikhoan_id IN (SELECT taikhoan_id FROM tbtaikhoan WHERE phongthi_id=$lstphongthi) ";
					$link.='&lstmucdo='.$lstphongthi;
					$smarty->assign('lstphongthi',$lstphongthi);
				}

				if(isset($_GET['lstkhoi']) && !empty($_GET['lstkhoi'])){
					$lstkhoi=$_GET['lstkhoi'];
					$strlay.=" AND taikhoan_id IN (SELECT taikhoan_id FROM tbtaikhoan WHERE khoi_id=$lstkhoi) ";
					$link.='&lstkhoi='.$lstkhoi;
					$smarty->assign('lstkhoi',$lstkhoi);
				}
				
				if(isset($_GET['lstlop']) && !empty($_GET['lstlop'])){
					$lstlop=$_GET['lstlop'];
					$strlay.=" AND taikhoan_id IN (SELECT taikhoan_id FROM tbtaikhoan WHERE lop_id=$lstlop) ";
					$link.='&lstlop='.$lstlop;
					$smarty->assign('lstlop',$lstlop);
				}
			}
			$strlay.=" ORDER BY taikhoan_id DESC";
			$danhsach=$lib->selectall($strlay);
			
			$smarty->assign('danhsach',$danhsach);//Khai báo biến tầng view
			$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
			$smarty->display('taikhoan/danhsach.html');
}

?>