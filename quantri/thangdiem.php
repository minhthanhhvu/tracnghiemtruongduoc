<?php
//Giả sử biến quy định để điều hướng phần quản lý sản phẩm: act
if(isset($_GET['act']))$act=$_GET['act']; else $act='';
$smarty->assign('act',$act);

if (isset($_GET['id']) && $act=='edit'){
	$detail=$lib->selectone("SELECT * FROM tbthangdiem WHERE thangdiem_id=".$_GET['id']);	
}
if (isset($_POST['lstsoluong']))
	$soluong=$_POST['lstsoluong'];
elseif(isset($detail))
	$soluong=$detail['thangdiem_soluong'];
else
	$soluong=2;
$smarty->assign('soluong',$soluong);

//Sử dụng lệnh rẽ nhánh switch case để hiển thị từng phần quản lý sản phẩm
switch($act)
{
	case 'add': 
				if(isset($_POST['btnup'])){
					$tieude=$_POST['txttieude'];
					$soluong=$_POST['lstsoluong'];
					$trangthai=$_POST['rdtrangthai'];
					$tenmuc=$_POST['tenmuc'];
					$thapnhat=$_POST['thapnhat'];
					$caonhat=$_POST['caonhat'];
					if (isset($_POST['chkdat']))
						$dat=$_POST['chkdat'];
					else
						$dat=array();
					
					//Xử lý cập nhật dữ liệu trên vào CSDL luôn
					$query="INSERT INTO tbthangdiem (thangdiem_tieude, thangdiem_soluong, thangdiem_trangthai) VALUES (?, ?, ?)";
					$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
					$re->bindParam(1,$tieude);
					$re->bindParam(2,$soluong);
					$re->bindParam(3,$trangthai);
					$re->execute();
					
					//Thêm đáp án vào CSDL
					$id=$lib->pdo->lastInsertId();
					foreach ($tenmuc as $key=>$value){
						if(in_array($key,$dat))
							$checkdat=1;
						else
							$checkdat=0;
						$query="INSERT INTO tbxeploai (thangdiem_id, xeploai_tieude, xeploai_thapnhat, xeploai_caonhat, xeploai_dat) VALUES (?, ?, ?, ?, ?)";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$id);
						$re->bindParam(2,$tenmuc[$key]);
						$re->bindParam(3,$thapnhat[$key]);
						$re->bindParam(4,$caonhat[$key]);
						$re->bindParam(5,$checkdat);
						$re->execute();
					}
					$lib->redirect('index.php?view=thangdiem');
				}
					
				$smarty->assign('action','index.php?view=thangdiem&act=add');
				$smarty->display('thangdiem/form.html');
				break;
	case 'edit': 
				//Lấy biến id trên URL xuống
				if(isset($_GET['id']))
				{
					$id=$_GET['id'];
					if(isset($_POST['btnup'])){
						$tieude=$_POST['txttieude'];
						$soluong=$_POST['lstsoluong'];
						$trangthai=$_POST['rdtrangthai'];
						$tenmuc=$_POST['tenmuc'];
						$thapnhat=$_POST['thapnhat'];
						$caonhat=$_POST['caonhat'];
						if (isset($_POST['chkdat']))
							$dat=$_POST['chkdat'];
						else
							$dat=array();
						
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="UPDATE tbthangdiem SET thangdiem_tieude=?, thangdiem_soluong=?, thangdiem_trangthai=? WHERE thangdiem_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$tieude);
						$re->bindParam(2,$soluong);
						$re->bindParam(3,$trangthai);
						$re->bindParam(4,$id);
						$re->execute();
						
						//Xóa xếp loại cũ
						$query="DELETE FROM tbxeploai WHERE thangdiem_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$id);
						$re->execute();
						
						// Cập nhật đáp án vào CSDL
						foreach ($tenmuc as $key=>$value){
							if(in_array($key,$dat))
								$checkdat=1;
							else
								$checkdat=0;
							$query="INSERT INTO tbxeploai (thangdiem_id, xeploai_tieude, xeploai_thapnhat, xeploai_caonhat, xeploai_dat) VALUES (?, ?, ?, ?, ?)";
							$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
							$re->bindParam(1,$id);
							$re->bindParam(2,$tenmuc[$key]);
							$re->bindParam(3,$thapnhat[$key]);
							$re->bindParam(4,$caonhat[$key]);
							$re->bindParam(5,$checkdat);
							$re->execute();
						}
						$lib->redirect('index.php?view=thangdiem');
					}
					$detail=$lib->selectone("SELECT * FROM tbthangdiem WHERE thangdiem_id=$id");
					$smarty->assign('detail',$detail);//Khai báo biến tầng View
					
					$smarty->assign('action','index.php?view=thangdiem&act=edit&id='.$id);
					$smarty->display('thangdiem/form.html');
				}
				break;
	case 'del': //Xử lý xóa
				if(isset($_GET['id'])){
					$id=$_GET['id'];	
					$lib->delete('tbthangdiem',$id);
				}elseif(isset($_POST['btndel'])){
					$list=$_POST['cbitem'];
					foreach($list as $id)
						$lib->delete('tbthangdiem',$id);
				}elseif(isset($_POST['btndelall'])){
					$lib->delete('tbthangdiem');
				}
				$lib->redirect('index.php?view=thangdiem');
				break;
	default: //Lập trình hiển thị danh sách
			$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
			$strlay="SELECT * FROM tbthangdiem ";
			$link='index.php?view=thangdiem';//Đường link phân trang
			if(isset($_GET['txtkey'])){
				$txtkey=$_GET['txtkey'];
				$strlay.=" WHERE thangdiem_tieude like '%$txtkey%'";
				$link.='&txtkey='.$txtkey;
				$smarty->assign('txtkey',$txtkey);
			}
			$strlay.=" ORDER BY thangdiem_id DESC";
			$danhsach=$lib->selectall($strlay);
			
			$smarty->assign('danhsach',$danhsach);//Khai báo biến tầng view
			$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
			$smarty->display('thangdiem/danhsach.html');
}

?>