<?php
//Giả sử biến quy định để điều hướng phần quản lý sản phẩm: act
if(isset($_GET['act']))$act=$_GET['act']; else $act='';
$smarty->assign('act',$act);

//Sử dụng lệnh rẽ nhánh switch case để hiển thị từng phần quản lý sản phẩm
switch($act)
{
	case 'add': 
				if(in_array('tohop_them',$login['quyen'])){
					if(isset($_POST['btnup'])){
						$tieude=$_POST['txttieude'];
						$mota=$_POST['txtmota'];
						$trangthai=$_POST['rdtrangthai'];
						
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="INSERT INTO tbtohop (tohop_tieude, tohop_trangthai, tohop_mota) VALUES (?, ?, ?)";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$tieude);
						$re->bindParam(2,$trangthai);
						$re->bindParam(3,$mota);
						$re->execute();
						$lib->redirect('index.php?view=tohop');
					}
					$smarty->assign('action','index.php?view=tohop&act=add');
					$smarty->display('tohop/form.html');
				}
				break;
	case 'edit': 
				//Lấy biến id trên URL xuống
				if(isset($_GET['id']))
				{
					$id=$_GET['id'];
					if(isset($_POST['btnup'])){
						$tieude=$_POST['txttieude'];
						$mota=$_POST['txtmota'];
						$trangthai=$_POST['rdtrangthai'];
						
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="UPDATE tbtohop SET tohop_tieude=?, tohop_trangthai=?, tohop_mota=? WHERE tohop_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$tieude);
						$re->bindParam(2,$trangthai);
						$re->bindParam(3,$mota);
						$re->bindParam(4,$id);
						$re->execute();
						$lib->redirect('index.php?view=tohop');
					}
				
					$detail=$lib->selectone("SELECT * FROM tbtohop WHERE tohop_id=$id");
					$smarty->assign('detail',$detail);//Khai báo biến tầng View
					
					$smarty->assign('action','index.php?view=tohop&act=edit&id='.$id);
					$smarty->display('tohop/form.html');
				}
				break;
	case 'del': //Xử lý xóa
				if(isset($_GET['id'])){
					$id=$_GET['id'];	
					$lib->delete('tbtohop',$id);
				}elseif(isset($_POST['btndel'])){
					$list=$_POST['cbitem'];
					foreach($list as $id)
						$lib->delete('tbtohop',$id);
				}elseif(isset($_POST['btndelall'])){
					$lib->delete('tbtohop');
				}
				$lib->redirect('index.php?view=tohop');
				break;
	default: //Lập trình hiển thị danh sách
			$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
			$strlay="SELECT * FROM tbtohop ";
			$link='index.php?view=tohop';//Đường link phân trang
			if(isset($_GET['txtkey'])){
				$txtkey=$_GET['txtkey'];
				$strlay.=" WHERE tohop_tieude like '%$txtkey%'";
				$link.='&txtkey='.$txtkey;
				$smarty->assign('txtkey',$txtkey);
			}
			$strlay.=" ORDER BY tohop_id DESC";
			$danhsach=$lib->selectall($strlay);
			
			$smarty->assign('danhsach',$danhsach);//Khai báo biến tầng view
			$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
			$smarty->display('tohop/danhsach.html');
}

?>