<?php
//Giả sử biến quy định để điều hướng phần quản lý sản phẩm: act
if(isset($_GET['act']))$act=$_GET['act']; else $act='';
$smarty->assign('act',$act);

//Sử dụng lệnh rẽ nhánh switch case để hiển thị từng phần quản lý sản phẩm
switch($act)
{
	case 'add': 
				if(isset($_POST['btnup'])){
					$tieude=$_POST['txttieude'];
					$trangthai=$_POST['rdtrangthai'];
					
					//Xử lý cập nhật dữ liệu trên vào CSDL luôn
					$query="INSERT INTO tbtruong (truong_tieude, truong_trangthai) VALUES (?, ?)";
					$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
					$re->bindParam(1,$tieude);
					$re->bindParam(2,$trangthai);
					$re->execute();
					$lib->redirect('index.php?view=truong');
				}
				$smarty->assign('action','index.php?view=truong&act=add');
				$smarty->display('truong/form.html');
				break;
	case 'edit': 
				//Lấy biến id trên URL xuống
				if(isset($_GET['id']))
				{
					$id=$_GET['id'];
					if(isset($_POST['btnup'])){
						$tieude=$_POST['txttieude'];
						$trangthai=$_POST['rdtrangthai'];
						
						//Xử lý cập nhật dữ liệu trên vào CSDL luôn
						$query="UPDATE tbtruong SET truong_tieude=?, truong_trangthai=? WHERE truong_id=?";
						$re = $lib->pdo->prepare($query);//Chạy lệnh chờ
						$re->bindParam(1,$tieude);
						$re->bindParam(2,$trangthai);
						$re->bindParam(3,$id);
						$re->execute();
						$lib->redirect('index.php?view=truong');
					}
				
					$detail=$lib->selectone("SELECT * FROM tbtruong WHERE truong_id=$id");
					$smarty->assign('detail',$detail);//Khai báo biến tầng View
					
					$smarty->assign('action','index.php?view=truong&act=edit&id='.$id);
					$smarty->display('truong/form.html');
				}
				break;
	case 'del': //Xử lý xóa
				if(isset($_GET['id'])){
					$id=$_GET['id'];	
					$lib->delete('tbtruong',$id);
				}elseif(isset($_POST['btndel'])){
					$list=$_POST['cbitem'];
					foreach($list as $id)
						$lib->delete('tbtruong',$id);
				}elseif(isset($_POST['btndelall'])){
					$lib->delete('tbtruong');
				}
				$lib->redirect('index.php?view=truong');
				break;
	default: //Lập trình hiển thị danh sách
			$lib->p=$_SESSION['caidat']['phantrang'];//Truyền số phần tử trên 1 trang
			$strlay="SELECT * FROM tbtruong ";
			$link='index.php?view=truong';//Đường link phân trang
			if(isset($_GET['txtkey'])){
				$txtkey=$_GET['txtkey'];
				$strlay.=" WHERE truong_tieude like '%$txtkey%'";
				$link.='&txtkey='.$txtkey;
				$smarty->assign('txtkey',$txtkey);
			}
			$strlay.=" ORDER BY truong_id DESC";
			$danhsach=$lib->selectall($strlay);
			
			$smarty->assign('danhsach',$danhsach);//Khai báo biến tầng view
			$smarty->assign('link',$link.'&n=');//Khai báo biến tầng view
			$smarty->display('truong/danhsach.html');
}

?>