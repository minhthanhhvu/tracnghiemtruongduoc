/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.extraPlugins = 'eqneditor';
	
	config.filebrowserBrowseUrl = '/templates/admin/ckfinder/ckfinder.html';
	config.filebrowserImageBrowseUrl = '/templates/admin/ckfinder/ckfinder.html?type=Images';
	config.filebrowserFlashBrowseUrl = '/templates/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
	config.filebrowserImageUploadUrl = '/templates/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
	config.filebrowserFlashUploadUrl = '/templates/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
	
	//Cấu hình trên web
	/*
	config.filebrowserBrowseUrl = '/templates/admin/ckfinder/ckfinder.html';
	config.filebrowserImageBrowseUrl = '/templates/admin/ckfinder/ckfinder.html?type=Images';
	config.filebrowserFlashBrowseUrl = '/templates/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
	config.filebrowserImageUploadUrl = '/templates/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
	config.filebrowserFlashUploadUrl = '/templates/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
	*/
};
